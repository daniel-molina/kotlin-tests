#!/bin/bash
averagePlotScript="scripts/plotChangingCalls.gnu"
totalPlotScript="scripts/plotChangingTotalProducts.gnu"
ratioPlotScript="scripts/plotChangingProductsOrdersRatio.gnu"
auxPlotScript="scripts/plotAux.gnu"
manageOptsBashScript="scripts/manage_opts.sh"

version="0.1.1"

source "$manageOptsBashScript"

setDefaultArg "-N" 3
setDefaultArg "-c" 1
setDefaultArg "-p" 100
setDefaultArg "-o" 100
setDefaultArg "-t" 524288

setDefaultArg "-g" "./"
setDefaultArg "-d" "./output/"
# Default directories created later only if needed

errorIfArgIsNotNatural "-N"
errorIfArgIsNotNatural "-c"
errorIfArgIsNotNatural "-p"
errorIfArgIsNotNatural "-o"
errorIfArgIsNotNatural "-t"
setArgNextPowerOf2 "-t"

usage() {
    echo "Usage: $0 [options] [tests] [executables] [solutions] [plots]

Tests:
  Must be one or more of these (abbreviations are valid):
    average     Changing Calls test
    total       Changing Total Products test
    ratio       Changing Products-Orders Ratio test
  Not specifying any of them is equivalent to pass all of them in the order
  'average total ratio'.

Executables:
  Must be one or more of these (abbreviations or lower-case are valid):
    JVM             tests results of the the Java Virtual Machine
    opt_native      tests results of the optimized Native/Kotlin executable
    native          tests results of the non-optimized Native/Kotlin executable
  Provided order (from left to right) will be respected when plotting.
  That affects which curves are over others and their vertical order in the
  key. Not specifying any of them is equivalent to pass all of them in the
  order 'JVM opt_native native'.

Solutions:
  Must be one or more of these:
    0   usingFlatMap solution
    1   copyingMax2GrowingList solution
    2   copyingMax2InitializedList solution
    3   accumulator solution
    4   accumulatorFold solution
    5   usingFlatMapWithSeq solution
  Provided order (from left to right) will be respected when plotting.
  That affects which curves are over others and their vertical order in the
  key. Note that it is relevant only if -x option is not passed.
  Not specifying any of them is equivalent to pass all of them in the order
  '0 1 2 3 4 5'. Option -0 (see below) modify this behavior.

Plots:
  Must be one or more of this (abbeviations or lower-case are valid):
    fun         Function (solution) execution time
    mem         Maximum resident set size
    prog        Program (test) execution time (System time + User time)
    CPU         Percentage of CPU used by the test calculated as
                100*(System time + User time)/(Real time)
  If not specified, all are considered.

About Y-axis of the plots:
    * It is possible to modify the range of Y-axis with option -Y.
      If plot is time or prog, values must be indicated in milliseconds
      (it applies even if Y-axis is displayed in seconds).
      If plot is mem, values must be indicated in megabytes.
      If plot is CPU, values must be indicated as a percentage.
    * If more than 2 decades are shown, log-scale is used for Y-axis.
      If -Y option is used, log-scale is used also when the range is bigger
      than 1 decade. This rule should produce always a reasonable plot.
    * If you want to override previous rule, use -l option.

Options:
  -g --graphs-dir <dir>   Specify a different directory where directories
                          containing the graphs will be created.
                          Default: Current directory.

  -d --results-dir <dir>  Specify a directory where to look for results to
                          create the graphs. Default: './output/'.

  -k --key <x,y>          Change position of the legend to (x, y).
                          Only one plot can be specified if this option is
                          used. y must be indicated in the same units described
                          above for y_min, y_max. By default, top left is used.

  -f --font-size <n>      Change font size of the graph. Useful if the key
                          is so big that it does not fit in an empty space.
                          Must be between 10 and 100. Default: 40.

  -e --error-lines        Include error lines in the plots. Not possible for
                          'CPU' plot.

  -l --logscale <0|1>     Force lin or log-scale for Y-axis. If parameter is 1,
                          logscale is used. If 0, linear scale.
                          Read note about Y-axis range above for more details.

  -Y --y-range <min,max>  Modify the range of Y-axis of the specified plot.
                          If used, only one plot can be specified.
                          Read note about Y-axis range above for more details.

  -s --group-solutions    Generate graphs in which different solutions appear
                          grouped, characterized by a single executable. Graphs
                          with different grouping will be included only if
                          options '-x' and/or '-m' are present.

  -x --group-executables  Generate graphs in which different executables appear
                          grouped, characterized by a single solution. Graphs
                          with different grouping will be included only if
                          options '-s' and/or '-m' are present.

  -m --merge              Generate graphs in which solutions and executables
                          appear together. Graphs with different grouping will
                          be included only if options '-s' and/or '-x' are
                          present.

  -N --realizations <n>   Number of times the test was run. Default: 3.

  -c --calls <n>          Number of times that solution was run in each test
                          (used in tests \"total\" and \"ratio\"). Default: 1.

  -p --prods <n>          Number of products per order used
                          (used in test \"average\"). Default: 100.

  -o --orders <n>         Number of orders used (used in test \"average\").
                          Default: 100.

  -t --total-prods <n>    Number of total products (used in test \"ratio\").
                          Default: 524288.

  -0 --skip-solution-0    Plot every solution except '0' in the default order
                          '1 2 3 4 5'.
                          It is specially useful because solution '0' usually
                          display quite different results than other solutions.
                          If solution 0 is passed along '-0', all solutions are
                          plotted in the order (1 2 3 4 5 0), independently of
                          the relative positions of '-0' and '0'.
                          Any other solution than '0' is incompatible with
                          '-0'.

  -C --compare            Plot relative values instead of absolute values. The
                          values represented by an executable and/or solution
                          (depending on the grouping) used as reference will be
                          plotted at y=1 and the values associated to the rest
                          will be divided by the value of the reference at the
                          same x value. In principle, the executable and/or
                          solution used as reference will be the one(s) which,
                          appearing in the graph, is/are found first in the
                          options passed to the program. Since 'CPU' and 'prog'
                          plots can skip values coming from short program
                          execution times, it has no sense to select an
                          executable and/or solution with all values skipped.
                          Instead, the first executable and/or solution that
                          appear(s) at least partially in every graph that
                          would include the same executables and/or solutions
                          if no skipping were done, will be used as the
                          reference. If you want to force an executable and/or
                          solution to be the reference in some plots but it is
                          totally skipped in 'CPU' and maybe 'prog', do not
                          include 'CPU' nor maybe 'prog' along with the plot(s)
                          of interest. Values associated to x-positions that
                          the reference does not include cannot be compared and
                          will be excluded. Probably you want to use as a
                          reference some results which cover a broad ranges to
                          include as much points as possible, even though it
                          can be the case that none executable nor/or solution
                          include(s) totally the range of the others.

  -q --quiet              Do not print warnings if options '-c', '-p', '-o' or
                          '-t' will not be effectively applied.

  -h --help               Show this help text and exit.

  -v  --version           Print version information and exit.
"
    exit
}

version() {
    echo "plot.sh $version"
    exit
}

# ------ Processing arguments

shortOpts="g:d:k:f:el:Y:sxmN:c:p:o:t:0Cqhv"
longOpts="graphs-dir:,results-dir:,key:,font-size:,error-lines,logscale:"
longOpts+=",y-range:,group-solutions,group-executables,merge,realizations:"
longOpts+=",calls:,prods:,orders:,total-prods:skip-solution-0,compare,quiet"
longOpts+=",help,version"

if ! opts=$(getopt -n "$0" -o "$shortOpts" --long "$longOpts" -- "$@"); then
    exit 1
fi

eval set -- "$opts"

sols=()
tests=()
execs=()
plots=()
while :
do
    case "$1" in
        -g | --graphs-dir)
            addDistinctOpt "-g" "$2"
            errorIfCannotWriteFilesInArg "-g"
            shift 2 ;;

        -d | --results-dir)
            addDistinctOpt "-d" "$2"
            errorIfArgIsNotAnExistingDir "-d"
            shift 2 ;;

        -k | --key)
            addDistinctOpt "-k" "$2"
            # Report format and value errors after knowing if -k option is
            # allowed
            shift 2 ;;

        -f | --font-size)
            addDistinctOpt "-f" "$2"
            errorIfArgIsNotNatural "-f"
            getArg fs "-f"
            if [[ $fs -lt 10 || $fs -gt 100 ]]; then
                errcho "Argument of option \"-f\" must be between 10 and 100."
            fi
            shift 2 ;;

        -e | --error-lines)
            addOpt "-e"
            shift ;;

        -l | --logscale)
            addDistinctOpt "-l" "$2"
            errorIfArgIsNot0Or1 "-l"
            shift 2 ;;

        -Y | --y-range)
            addDistinctOpt "-Y" "$2"
            # Report format and value errors after knowing if -Y option is
            # allowed
            shift 2 ;;

        -s | --group-sols-only)
            addOpt "-s"
            shift ;;

        -x | --group-execs-only)
            addOpt "-x"
            shift ;;

        -m | --merge-only)
            addOpt "-m"
            shift ;;

        -N | --realizations)
            addDistinctOpt "-N" "$2"
            errorIfArgIsNotNatural "-N"
            shift 2 ;;

        -c | --calls)
            addDistinctOpt "-c" "$2"
            errorIfArgIsNotNatural "-c"
            shift 2 ;;

        -p | --prods)
            addDistinctOpt "-p" "$2"
            errorIfArgIsNotNatural "-p"
            shift 2 ;;

        -o | --orders)
            addDistinctOpt "-o" "$2"
            errorIfArgIsNotNatural "-o"
            shift 2 ;;

        -t | --total-prods)
            addDistinctOpt "-t" "$2"
            errorIfArgIsNotNatural "-t"
            # Help the user who possibly indicates what was typed as x_max in
            # run_tests.sh
            setArgNextPowerOf2 "-t"
            shift 2 ;;

        -0 | --skip-solution-0)
            addDistinctOpt "-0"
            for i in '1' '2' '3' '4' '5'; do
                addDistinctOpt "$i"
                sols+=("$i")
            done
            shift ;;

        -C | --compare)
            addOpt "-C"
            shift ;;

        -q | --quiet)
            addOpt "-q"
            shift ;;

        -h | --help) usage ;;

        -v | --version) version ;;

        --)
            shift; break ;;

        *)
           errcho "This should not have happened... Argument that caused it: \"$1\"."
           exit 1 ;;
    esac
done

# Parse non-option parameters. Order matters.
# Strategy: assert that they are not passed before with an associative array
#           (through 'addDistinctOpt'). Then, add them to an indexed array.
for i in "$@"
do
    case "$i" in
        [0-5])
            addDistinctOpt "$i";          sols+=("$i")   ;;
        a|av|ave|aver|avera|averag|average)
            addDistinctOpt 'average';     tests+=('average')    ;;
        t|to|tot|tota|total)
            addDistinctOpt 'total';       tests+=('total')      ;;
        r|ra|rat|rati|ratio)
            addDistinctOpt 'ratio';       tests+=('ratio')      ;;
        j|jv|jvm|J|JV|JVM)
            addDistinctOpt 'JVM';         execs+=('JVM')        ;;
        o|op|opt|opt_|opt_n|opt_na|opt_nat|opt_nati|opt_nativ|opt_native)
            addDistinctOpt 'opt_native';  execs+=('opt_native')  ;;
        n|na|nat|nati|nativ|native)
            addDistinctOpt 'native';      execs+=('native')     ;;
        f|fu|fun)
            addDistinctOpt 'fun';         plots+=('fun')  ;;
        m|me|mem)
            addDistinctOpt 'mem';         plots+=('mem')  ;;
        p|pr|pro|prog)
            addDistinctOpt 'prog';        plots+=('prog') ;;
        c|cp|cpu|C|CP|CPU)
            addDistinctOpt 'CPU';         plots+=('CPU')  ;;
        *)
            errcho "Invalid non-option parameter: \"$i\".";;
    esac
done

# --------- Set default options

# Only create default directories if they were not provided
if ! isOptWithArgAdded "-g"; then
    errorIfCannotWriteFilesInArg "-g"
fi

if ! isOptWithArgAdded "-d"; then
    errorIfArgIsNotAnExistingDir "-d"
fi

# If no test is passed, all are selected
if [[ ${#tests[*]} -eq 0 ]]; then
    for i in 'average' 'total' 'ratio'; do
        addDistinctOpt "$i"
        tests+=("$i")
    done
fi

# If no execuatable is passed, all are selected
if [[ ${#execs[*]} -eq 0 ]]; then
    for e in 'JVM' 'opt_native' 'native'; do
        addDistinctOpt "$e"
        execs+=("$e")
    done
fi

# If no plot is passed, all are selected
if [[ ${#plots[*]} -eq 0 ]]; then
    for plot in 'fun' 'mem' 'prog' 'CPU'; do
        addDistinctOpt "$plot"
        plots+=("$plot")
    done
fi

# If no solution is passed, all are selected
if [[ ${#sols[*]} -eq 0 ]]; then
    for s in '0' '1' '2' '3' '4' '5'; do
        addDistinctOpt "$s"
        sols+=("$s")
    done
fi

# If no restriction on the grouping, all are selected.
if ! isOptWithoutArg "-s" && ! isOptWithoutArg "-x" \
        && ! isOptWithoutArg "-m"; then
    addOpt "-s"
    addOpt "-x"
    addOpt "-m"
fi

# ---------- Checking incompatible parameters

# ---------------- Strategy ----------------------
# Report a meaningnful error and exit.
# Don't report several errors at the same time.
# ------------------------------------------------

isRealOrErr() {
    if ! isReal "$1"; then
        errcho "$2 must be a valid number. Provided value: \"$1\"."
    fi
}

if isOptWithArg "-k"; then
    if [[ ${#plots[*]} -ne 1 ]] && ! isOptWithoutArg "-C"; then
        plotsString=$(prettyList "${plots[@]}")
        errcho "Error: Key position cannot be specified if more than one plot is indicated." \
            "Tip: Current arguments establish ${#plots[*]} plots: $plotsString."
    fi
    getArg key "-k"
    xKey=${key%,*}
    yKey=${key#*,}
    if [[ -z $xKey || -z $yKey ]]; then
        errcho "Key coordinates must be provided separated by a comma with no spaces: x_key,y_key."
    fi
    isRealOrErr "$xKey" "x_key"
    isRealOrErr "$yKey" "y_key"
fi

if isOptWithArg "-Y"; then
    if [[ ${#plots[*]} -ne 1 ]] && ! isOptWithoutArg "-C"; then
        plotsString=$(prettyList "${plots[@]}")
        errcho "Error: Y-range cannot be specified if more than one plot is indicated." \
            "Tip: Current arguments establish ${#plots[*]} plots: $plotsString."
    fi
    getArg yRange "-Y"
    yMin=${yRange%,*}
    yMax=${yRange#*,}
    if [[ -z $yMin || -z $yMax ]]; then
        errcho "Y-axis range ends must be provided separated by a comma with no spaces: y_min,y_max."
    fi
    isRealOrErr "$yMin" "y_min"
    isRealOrErr "$yMax" "y_max"
    isYRangeNull=$(echo | awk "{print $yMin == $yMax}")
    if [[ $isYRangeNull -eq 1 ]]; then
        errcho "y_min and y_max cannot be equal. Passed values are \"$yMin\" and \"$yMax\"."
    fi
fi

if isOptWithoutArg "-e" && isOptWithoutArg "-C"; then
    errcho "Options \"-e\" and \"-C\" are not compatible."
fi

if ! isOptWithoutArg "-q"; then
    if ! isOptWithoutArg "total" && ! isOptWithoutArg "ratio" && isOptWithArgAdded "-c"; then
        >&2 echo "WARNING: Specifying a number of calls only has sense for tests \"total\" and \"ratio\"."
    fi

    if ! isOptWithoutArg "average" && isOptWithArgAdded "-p"; then
        >&2 echo "WARNING: Specifying a number of products per order only has sense for test \"average\"."
    fi

    if ! isOptWithoutArg "average" && isOptWithArgAdded "-o"; then
        >&2 echo "WARNING: Specifying a number of orders only has sense for test \"average\"."
    fi

    if ! isOptWithoutArg "ratio" && isOptWithArgAdded "-t"; then
        >&2 echo "WARNING: Specifying a total number of products only has sense for test \"ratio\"."
    fi
fi

# ---------- Do the real stuff

# getExecsString <var_name> <exec1> [exec2] [exec3]
getExecsString() {
    if [[ $# -lt 2 ]]; then
        >&2 echo "Wrong number of arguments passed to ${FUNCNAME[0]}."
        exit 1
    fi
    local -n gESRef=$1 || wrongVarNameErr "$1"
    shift
    gESRef=
    for exec in "$@"; do
        case "$exec" in
            JVM)        gESRef+="JVM "         ;;
            opt_native) gESRef+="NativeOpt "   ;;
            native)     gESRef+="Native "      ;;
            *)
                >&2 echo "Wrong executable type passed to ${FUNCNAME[0]}."
                exit 1 ;;
        esac
    done
    gESRef=${gESRef%?}
}

# Warning: auxPlotScript must be correcly set before calling the
#          following functions

sedProg=
# addVar2List <var> <value>
# resetVarsList
#
# Construct a sed program to set values to variables
# It is finally applied with setVars
#
# It does not work if the variable is not in the script (commented is OK) 
# Using 's|||' instead of 's///' because '/' is used in directories.
#
# It modifies: sedProg
addVar2List() { sedProg+="s|^\\s*#*\\s*${1}\\s*=.*|${1} = ${2}|;"; }
resetVarsList() { sedProg=; }

# setVarsListInScript: Set variables included with addVar in the gnuplot script
#
# It reads: sedProg, auxPlotScript
updateScriptWithVarsList() { sed -i "$sedProg" "$auxPlotScript"; }

# newScript <base_script>
newAuxScript() { cp "$1" "$auxPlotScript"; }

# runScript
runScript() { gnuplot "$auxPlotScript"; }

# Set variables that are common to every test, exec and plot
addCommonVars2List() {
    if isOptWithArg '-Y'; then
        addVar2List "yMin" "$yMin"
        addVar2List "yMax" "$yMax"
    fi
    if isOptWithArg '-k'; then
        addVar2List "xKey" "$xKey"
        addVar2List "yKey" "$yKey"
    fi
    if isOptWithArg '-f'; then
        getArg fs "-f"
        addVar2List "fontSize" "$fs"
    fi
    if isOptWithoutArg '-e'; then
        addVar2List "plotErrorLines" "1"
    fi
    if isOptWithArg '-l'; then
        getArg log "-l"
        addVar2List "yLogscale" "$log"
    fi
    if isOptWithoutArg "-C"; then
        addVar2List "plotRelativeValues" "1"
    fi
    addVar2List "plots" "\"${plots[*]}\""
    # The rest must have a default
    getArg N "-N"
    addVar2List "N" "$N"
    getArg resultsDir "-d"
    addVar2List "inDir" "\"$resultsDir\""
}

# setGraphsDir <subfolder>
#
# Warning: <subfolder> MUST finish at '/'
setGraphsDir() {
    # Implementation assures that provided directories always finish at '/'
    # and files can be created on them.
    getArg graphsDir "-g"
    addVar2List "outDir" "\"${graphsDir}${1}\""
}

# plotSolsTogether <main_dir>
plotSolsTogether() {
    setGraphsDir "$1/one_graph_per_executable/"
    for exec in "${execs[@]}"; do
        getExecsString e "$exec"
        addVar2List "kotlinExecs" "\"$e\""
        addVar2List "sols" "\"${sols[*]}\""
        updateScriptWithVarsList
        runScript
        resetVarsList
    done
}

# plotExecsTogether <main_dir>
plotExecsTogether() {
    setGraphsDir "$1/one_graph_per_solution/"
    for sol in "${sols[@]}"; do
        addVar2List "sols" "\"$sol\""
        getExecsString es "${execs[@]}"
        addVar2List "kotlinExecs" "\"$es\""
        updateScriptWithVarsList
        runScript
        resetVarsList
    done
}

# plotSolsAndExecsTogether <main_dir>
plotSolsAndExecsTogether() {
    setGraphsDir "$1/"
    addVar2List "sols" "\"${sols[*]}\""
    getExecsString es "${execs[@]}"
    addVar2List "kotlinExecs" "\"$es\""
    updateScriptWithVarsList
    runScript
    resetVarsList
}

# plotCorrectlyGrouped <main_dir>
plotCorrectlyGrouped() {
    isOptWithoutArg "-s" && plotSolsTogether "$1"
    isOptWithoutArg "-x" && plotExecsTogether "$1"
    isOptWithoutArg "-m" && plotSolsAndExecsTogether "$1"
}

trap 'errcho "Execution stopped by SIGINT."' INT

if ! gnuplot --version >& /dev/null; then
    errcho "gnuplot command not found."
fi

for test in "${tests[@]}"; do
    if [[ $test = "average" ]]; then
        newAuxScript "$averagePlotScript"
        addCommonVars2List
        getArg o "-o"
        getArg p "-p"
        addVar2List "orders" "$o"
        addVar2List "prods" "$p"
        plotCorrectlyGrouped "$test"
    fi

    if [[ $test = "total" ]]; then
        newAuxScript "$totalPlotScript"
        addCommonVars2List
        getArg c "-c"
        addVar2List "calls" "$c"
        plotCorrectlyGrouped "$test"
    fi

    if [[ $test = "ratio" ]]; then
        newAuxScript "$ratioPlotScript"
        addCommonVars2List
        getArg c "-c"
        getArg t "-t"
        addVar2List "calls" "$c"
        addVar2List "totalProds" "$t"
        plotCorrectlyGrouped "${test}${t}"
    fi
done

rm "$auxPlotScript"
