#!/bin/bash

#------------------------------- Set executables -------------------------------
kotlincBin="kotlinc"
kotlincNativeBin="kotlinc-native"
javaBin="java"
#-------------------------------------------------------------------------------

version="0.1.1"

manageOptsBashScript="./scripts/manage_opts.sh"
manageExcerptDBBashScript="./scripts/manage_db.sh"
plotScript="./plot.sh"

source "$manageOptsBashScript"
source "$manageExcerptDBBashScript"

testCode="testListsOfListsFunctions.kt"
jarFile="testListsOfListsFunctions.jar"
nativeBin="testListsOfListsFunctions.kexe"
nativeBinOpt="testListsOfListsFunctionsOpt.kexe"
excerptDBname="excerpt.sqlite3"

cmdJVM="$javaBin -jar $jarFile"
cmdNative="./$nativeBin"
cmdNativeOpt="./$nativeBinOpt"

defaultxMaxAverage=512
defaultxMaxTotal=2048
# It will be set to arg of '-t' if that is passed and not x_max
defaultxMaxRatio=524288

nextPowerOf2 defaultxMaxAverage
nextPowerOf2 defaultxMaxTotal
nextPowerOf2 defaultxMaxRatio

setDefaultArg "-N" 3
setDefaultArg "-c" 1
setDefaultArg "-p" 100
setDefaultArg "-o" 100
# Default of -t will be set again if x_max is passed
setDefaultArg "-t" "$defaultxMaxRatio"

setDefaultArg "-g" "./"
setDefaultArg "-d" "./output/"
# Default directories created later if necessary

errorIfArgIsNotNatural "-N"
errorIfArgIsNotNatural "-c"
errorIfArgIsNotNatural "-p"
errorIfArgIsNotNatural "-o"

usage() {
    echo "Usage: $0 [options] [solutions] [executables] [tests]
   or: $0 [options] [solutions] [executables] <test> <x_max>

Run tests according to the specified parameters and generate graphs with the
results. Sucessive calls will reuse available results of previous executions,
not running unnecesary tests.

Solutions:
  Must be indicated with one or more of the following indices:
    0   usingFlatMap solution
    1   copyingMax2GrowingList solution
    2   copyingMax2InitializedList solution
    3   accumulator solution
    4   accumulatorFold solution
    5   usingFlatMapWithSeq solution
  Provided order (from left to right) will be respected when running the tests.
  Not specifying any of them is equivalent to pass all of them in the order
  '0 1 2 3 4 5'. Option -0 (see below) modify this behavior.

Executables:
  Must be one or more of these (abbeviations or lower-case are valid):
    JVM             run test in the Java Virtual Machine
    opt_native      run test with optimized Native/Kotlin executable
    native          run test with non-optimized Native/Kotlin executable
  Provided order (from left to right) will be respected when running the tests.
  Not specifying any of them is equivalent to pass all of them in the order
  'JVM opt_native native'.

Tests:
  Must be one or more of the following codenames (abbeviations are valid):
    average     Changing Calls test
    total       Changing Total Products test
    ratio       Changing Products-Orders Ratio test
  Provided order (from left to right) will be respected when running the tests.
  Not specifying any of them is equivalent to pass all of them in the order
  'average total ratio'.

x_max:
  This parameter lets you control the maximum value of the independent variable
  of a test according to the following rules:
    * If used, only one test can be specified.
    * If it is not a power of 2, the smallest power of 2 bigger than x_max will
      be used instead.
    * In the unusual case that you want 'x_max' smaller than 6, you can indicate
      it by including a '+' directly before the number, e.g. +4. Without the
      '+', it will be interpreted as a solution index, as explained above.
    * For test \"average\", 'x_max' sets the maximum number of calls tested.
      Default: 512.
    * For test \"total\", 'x_max' represents the maximum number of orders.
      Note that the graph represents in X-axis the total number of prducts,
      which is equal to the square of that quantity. Default: 2048.
    * For test \"ratio\", 'x_max' represents the maximum number of orders. The
      total number of products will be set automatically to this value in every
      call unless '-t' parameter is used. As a consequence, if '-t' is not used,
      different values of 'x_max' will not reuse previous results.
      Note: If you are going to create or modify plots by yourself with plot.sh,
      remember to specify correctly the total number of products (with parameter
      '-t') if you set 'x_max' (or '-t').
      Default: 524288 if '-t' is not used. If it is used, the value of '-t'.

Options:
  -g --graphs-dir <dir>   Specify a different directory where directories
                          containing the graphs will be created.
                          Default: Current directory.

  -d --results-dir <dir>  Specify a directory where results of the test will be
                          saved. Default: './output/'.

  -l --list               Show an excerpt of the tests that have been done and
                          exit. The excerpt shows only those tests compatible
                          with the rest of the options. An exception to this
                          rule is that, if '-N', '-c', '-o' or '-p' are not
                          specified, their default values are not used to filter
                          the output.

  -N --realizations <n>   Number of times to run the tests. Default: 3.

  -c --calls <n>          Number of times to run each solution in each test
                          (used in tests \"total\" and \"ratio\"). Default: 1.

  -p --prods <n>          Number of products per order
                          (used in test \"average\"). Default: 100.

  -o --orders <n>         Number of orders (used in test \"average\").
                          Default: 100.

  -t --total <n>          Number of total products (used in test \"ratio\").
                          Default: 'x_max' or default value of 'x_max' for
                          test 'ratio' (see above). If 'x_max' is provided, '-t'
                          must be greater than or equal to 'x_max'. Else,
                          'x_max' will be set equal to '-t'.

  -0 --skip-solution-0    Run the tests for every solution except '0' using the
                          order '1 2 3 4 5'.
                          It is specially useful because '0' is the slowest and
                          most memory-demanding solution.
                          If solution '0' is passed along '-0', all solutions
                          are used in the order '1 2 3 4 5 0' independently of
                          the relative positions of '-0' and '0' Any other
                          solution than '0' is incompatible with '-0'.

  -f --force              Run any well-specified test. By default, this
                          program does not allow you to run some tests that
                          demand a lot of memory. This option prevents that.
                          WARNING: Use it at your own risk! 'native' and
                          'opt_native' are known to not stop execution even
                          if they use all available memory.

  -P --process-only       Process previously obtained results that are
                          compatible with passed parameters and include them in
                          the excerpt database. If no 'x_max' is specified, the
                          program will process any available result for the 'x'
                          variable, independently of the default values for each
                          test. Note that default values for other parameters
                          apply, including '-t'. This option should not be
                          needed unless you erased the excerpt database. No test
                          is run if this option is present.

  -E --erase-db           Erase the excerpt database. Note that it does not
                          delete any result. Any obtained result can be included
                          again in the excerpt database using option '-P'.
                          Removing specific entries according to parameters
                          may be implemented in the future.

  -h --help               Show this help text and exit.

  -v  --version           Print version information and exit.
"
    exit
}

version() {
    echo "run_tests.sh $version"
    exit
}

shortOpts="g:d:lN:c:p:o:t:0fPEhv"
longOpts="graphs-dir:,results-dir:,list,realizations:,calls:,prods:"
longOpts+=",orders,total,skip-solution-0,force:,process-only,erase-db,help"
longOpts+=",version"

# ------ Processing arguments
if ! opts=$(getopt -n "$0" -o "$shortOpts" --long "$longOpts" -- "$@"); then
    exit 1
fi

eval set -- "$opts"

sols=()
tests=()
execs=()
while :
do
    case "$1" in
        -g | --graphs-dir)
            addDistinctOpt "-g" "$2"
            shift 2 ;;

        -d | --results-dir)
            addDistinctOpt "-d" "$2"
            shift 2 ;;

        -l | --list)
            addOpt "-l"
            shift ;;

        -N | --realizations)
            addDistinctOpt "-N" "$2"
            errorIfArgIsNotNatural "-N"
            shift 2 ;;

        -c | --calls)
            addDistinctOpt "-c" "$2"
            errorIfArgIsNotNatural "-c"
            shift 2 ;;

        -p | --prods)
            addDistinctOpt "-p" "$2"
            errorIfArgIsNotNatural "-p"
            shift 2 ;;

        -o | --orders)
            addDistinctOpt "-o" "$2"
            errorIfArgIsNotNatural "-o"
            shift 2 ;;

        -t | --total)
            addDistinctOpt "-t" "$2"
            errorIfArgIsNotNatural "-t"
            getArg passedt "-t"
            setArgNextPowerOf2 "-t"
            getArg usedt "-t"
            msg="Passed total number of products is $passedt. "
            msg+=" It has been set to: $usedt."
            echo "$msg"
            shift 2 ;;

        -0 | --skip-solution-0)
            addDistinctOpt "-0"
            for i in '1' '2' '3' '4' '5'; do
                addDistinctOpt "$i"
                sols+=("$i")
            done
            shift ;;

        -f | --force)
            addOpt "-f"
            shift ;;

        -P | --process-only)
            addOpt "-P"
            shift ;;

        -E | --erase-db)
            addOpt "-E"
            shift ;;

        -h | --help) usage ;;

        -v | --version) version ;;

        --)
            shift; break ;;

        *)
            errcho "This should not have happened... Argument that caused it: $1."
            ;;
    esac
done

for i in "$@"
do
    case "$i" in
        [0-5])
            addDistinctOpt "$i";          sols+=("$i")          ;;
        a|av|ave|aver|avera|averag|average)
            addDistinctOpt 'average';     tests+=('average')    ;;
        t|to|tot|tota|total)
            addDistinctOpt 'total';       tests+=('total')      ;;
        r|ra|rat|rati|ratio)
            addDistinctOpt 'ratio';       tests+=('ratio')      ;;
        j|jv|jvm|J|JV|JVM)
            addDistinctOpt 'JVM';         execs+=('JVM')        ;;
        o|op|opt|opt_|opt_n|opt_na|opt_nat|opt_nati|opt_nativ|opt_native)
            addDistinctOpt 'opt_native';  execs+=('opt_native') ;;
        n|na|nat|nati|nativ|native)
            addDistinctOpt 'native';      execs+=('native')     ;;
        *)
            # Here we manage x_max and any non-desired non-option parameters.
            # Do not report errors here. Wait to parse every option to report a
            # meaningful reason. It is, do not tell the user about a wrong
            # format of x_max if you do not know if x_max is allowed.
            if isOptWithArg "x_max"; then
                # It does not matter if extraParameter is overwritten here.
                # One offending parameter is enough to report and it is safer
                # than storing an unknown number of elements.
                extraParameter="$i"
            else
                addDistinctOpt 'x_max' "$i"
            fi ;;
    esac
done

# --------- Set default options

# If no test is passed, all are selected
if [[ ${#tests[*]} -eq 0 ]]; then
    for test in 'average' 'total' 'ratio'; do
        addDistinctOpt "$test"
        tests+=("$test")
    done
fi

# If no execuatable is passed, all are selected
if [[ ${#execs[*]} -eq 0 ]]; then
    for e in 'JVM' 'opt_native' 'native'; do
        addDistinctOpt "$e"
        execs+=("$e")
    done
fi

# If no solution is passed, all are selected
if [[ ${#sols[*]} -eq 0 ]]; then
    for i in '0' '1' '2' '3' '4' '5'; do
        addDistinctOpt "$i"
        sols+=("$i")
    done
fi

# Set a default action to do conditionals simpler to write and think
if ! { isOptWithoutArg "-P" || isOptWithoutArg "-l" \
        || isOptWithoutArg "-E"; }; then
    addOpt "default_action"
fi

# ---------- Checking incompatible parameters

# ---------------- Strategy ----------------------
# Report a meaningnful error and exit.
# Don't report several errors at the same time.
# ------------------------------------------------

# Note: Functions will assume that only one test was specified if x_max was
#       passed.

# x_max

if isOptWithArgAdded "x_max"; then
    getArg xMaxRaw "x_max"
    if [[ ${#tests[*]} -ne 1 ]]; then
        testsString=$(prettyList "${tests[@]}")
        errcho "Error: Invalid non-option parameter: \"$xMaxRaw\"." \
            "Tip: If you want to specify x_max, choose a single test." \
            "     Current arguments establish ${#tests[*]} tests: $testsString."
    fi
    errorIfArgIsNotNatural 'x_max'
    # Setting xMax to a power of 2 is strictly needed for test 'ratio'.
    # However, to get the benefits of reusing results from previous runs of
    # this program, it is better to use only powers of 2.
    setArgNextPowerOf2 "x_max"
    getArg xMax "x_max"
    setDefaultArg "-t" "$xMax"
    # Inform the user about effective x_max
    msgMax="Passed x_max is $xMaxRaw. Maximum"
    case "${tests[0]}" in
        average) echo "$msgMax number of calls has been set to: $xMax." ;;
        total) echo "$msgMax total number of products is $xMax^2." ;;
        ratio) echo "$msgMax number of orders has been set to $xMax." ;;
        *) >&2 echo "Internal error: Unknown test."; exit 1 ;;
    esac
fi

# If extraParameter exists, xMax too and it has a correct value
if [[ -n ${extraParameter+x} ]]; then
    errcho "Error: Invalid non-option parameter: \"$extraParameter\"." \
        "Tip: x_max has been already correctly passed. Provided value: \"$xMax\"."
fi

# calls, prods, orders, total prods warnings

if ! isOptWithoutArg "total" && ! isOptWithoutArg "ratio" && isOptWithArgAdded "-c"; then
    >&2 echo "WARNING: Specifying a number of calls only has sense for tests \"total\" and \"ratio\"."
fi

if ! isOptWithoutArg "average" && isOptWithArgAdded "-p"; then
    >&2 echo "WARNING: Specifying a number of products per order only has sense for test \"average\"."
fi

if ! isOptWithoutArg "average" && isOptWithArgAdded "-o"; then
    >&2 echo "WARNING: Specifying a number of orders only has sense for test \"average\"."
fi

if ! isOptWithoutArg "ratio" && isOptWithArgAdded "-t"; then
    >&2 echo "WARNING: Specifying a total number of products only has sense for test \"ratio\"."
fi

# total products
# Condition t >= xMax only checked if 'ratio' test is selected and
# xMax and '-t' passed.

if isOptWithoutArg "ratio" && isOptWithArgAdded "-t"; then
    getArg totalProds "-t"
    if isOptWithArgAdded "x_max"; then
        getArg xMax "x_max"
        if [[ $totalProds -lt $xMax ]]; then
            msg="Error: Option \"-t\" must be greater than or equal to "
            msg+="\"x_max\"."
            errcho "$msg"
        fi
    else
        defaultxMaxRatio="$totalProds"
    fi
fi

# Multiple actions

if isOptWithoutArg "-E" && isOptWithoutArg "-l"; then
    errcho "Options -E and -l are not compatible."
fi
if isOptWithoutArg "-P" && isOptWithoutArg "-l"; then
    errcho "Options -P and -l are not compatible."
fi
if isOptWithoutArg "-E" && isOptWithoutArg "-P"; then
    errcho "Options -E and -P are not compatible."
fi

if isOptWithArgAdded "-g" && { isOptWithoutArg "-E" \
        || isOptWithoutArg "-l" || isOptWithoutArg "-P"; }; then
    >&2 echo "Warning: Option \"-g\" will not be used."
fi

# ------- Check and set the root directories and DB

# Help: Appart from -v and -h, there are 4 main actions:
# default_action: It can create results and graphs and can write in the DB.
# -E:             It removes an existing DB.
# -l:             It lists an existing DB.
# -P:             It can write in the DB,

# Need of current existence of resultsDir (where DB or results should be)
if isOptWithoutArg "-P" || isOptWithoutArg "-l" || isOptWithoutArg "-E"; then
    errorIfArgIsNotAnExistingDir "-d"
fi
# Need of creating files in resultsDir
if isOptWithoutArg "default_action" || isOptWithoutArg "-P"; then
    errorIfCannotWriteFilesInArg "-d"
fi
# Need of creating files in graphsDir
if isOptWithoutArg "default_action"; then
    errorIfCannotWriteFilesInArg "-g"
fi

# Set path of DB (do not init yet)
getArg resultsDir "-d"
DBPath="${resultsDir}${excerptDBname}"
setDBPath "$DBPath"

# Need of current existence of excerpt DB
if isOptWithoutArg "-E"; then
    [[ -f $DBPath ]] || errcho "No excerpt database found."
    [[ -w $DBPath ]] || errcho "Excerpt database cannot be deleted."
fi
if isOptWithoutArg "-l"; then
    if ! [[ -f $DBPath ]]; then
        errcho "Error: Excerpt database not found in directory: \"${resultsDir}\"." \
            "That means that no results were saved there or database was erased." \
            "If it is the later, you can create a new one using option '-P'."
    fi
    [[ -r $DBPath ]] || errcho "Excerpt database cannot be read."
fi
# Need of a writable excerpt DB, and if it does not exist, create it (now is OK)
if isOptWithoutArg "default_action" || isOptWithoutArg "-P"; then
    if [[ -f $DBPath ]]; then
        [[ -w $DBPath ]] || errcho "Excerpt database cannot be written."
    else
        DBCreatedInThisExecution=1
    fi
    initDBIfNeeded
fi

# ---- Manage options -E, -l and abscence of -f

# Manage option -E
if isOptWithoutArg "-E"; then
    rm "${resultsDir}${excerptDBname}"
    exit
fi

# Manage -l option

# errorLongExecTime <details> <value>
errorLongExecTime() {
    errcho "Error: Tests for $1 bigger than $2 can last a long time." \
        "Include -f option if you want to force execution of selected tests."
}
# errorMuchMemory <details> <value>
errorMuchMemory() {
    errcho "Error: Tests for $1 bigger than $2 occupie a lot of memory (and it increases exponentially)." \
        "Include -f option if you want to force execution of selected tests."
}
if isOptWithoutArg "-l"; then
    # The loop is used to preserve the order provided by the user for the tests
    isOptWithArgAdded "-N" && getArg N "-N" || N="%"
    isOptWithArgAdded "x_max" && getArg xMax "x_max" || xMax="%"
    for test in "${tests[@]}"; do
        case "$test" in
            total)
                isOptWithArgAdded "-c" && getArg c "-c" || c="%"
                totalDisplay "$N" execs "$c" sols "$xMax" ;;
            average)
                isOptWithArgAdded "-o" && getArg o "-o" || o="%"
                isOptWithArgAdded "-p" && getArg p "-p" || p="%"
                averageDisplay "$N" execs "$o" "$p" sols "$xMax" ;;
            ratio)
                isOptWithArgAdded "-c" && getArg c "-c" || c="%"
                isOptWithArgAdded "-t" && getArg t "-t" || t="%"
                ratioDisplay "$N" execs "$c" sols "$t" "$xMax" ;;
            *)
                >&2 echo "Internal error: Wrong test in ${FUNCNAME[0]}."
                exit 1 ;;
        esac
    done
    exit
fi

# wrongVarNameErr <wrong_var_name>
wrongVarNameErr() {
    >&2 echo "Wrong argument passed to ${FUNCNAME[1]}. \"$1\" is not a variable name."
    exit 1
}

# getxMax <var_name> <test>
#
# Saves in variable named <var_name> x_max passed by user or,
# if it was not passed, the default x_max value for <test>.
#
# It can read from the environment: option "x_max"
getxMax() {
    local -n gxMRef=$1 || wrongVarNameErr "$1"
    if isOptWithArgAdded "x_max"; then
        getArg gxMRef "x_max"
    else
        case "$2" in
            total)   gxMRef="$defaultxMaxTotal" ;;
            average) gxMRef="$defaultxMaxAverage" ;;
            ratio)   gxMRef="$defaultxMaxRatio" ;;
            *) >&2 echo "Wrong test passed to ${FUNCNAME[0]}."; exit 1 ;;
        esac
    fi
}


# Manage absence of -f option
# It is applied from more to less restrictive

if isOptWithoutArg "default_action" && ! isOptWithoutArg "-f"; then
    # There are many possibilities, let us obtain the important quantities in
    # an explicit way.

    # Biggest representable integer in bash for 64 bits is 9223372036854775807,
    # which integer square root is 3037000499.
    # It is safer to use bc to calculate totalProds than bash arithmetic.
    # I will use it also for calls for symmetry.
    # o*p or xMaxtotal^2 will not be used later.
    totalProds=0 calls=0
    if isOptWithoutArg "average"; then
        getArg o "-o"
        getArg p "-p"
        getxMax xMaxaverage "average"
        totalProds=$(echo "if($totalProds > $o * $p) $totalProds else $o * $p" | bc )
        calls=$(echo "if($calls > $xMaxaverage) $calls else $xMaxaverage" | bc )
    fi
    if isOptWithoutArg "total"; then
        getArg c "-c"
        getxMax xMaxtotal "total"
        totalProds=$(echo "if($totalProds > $xMaxtotal^2) $totalProds else $xMaxtotal^2" | bc )
        calls=$(echo "if($calls > $c) $calls else $c" | bc )
    fi
    if isOptWithoutArg "ratio"; then
        getArg c "-c"
        getArg t "-t"
        totalProds=$(echo "if($totalProds > $t) $totalProds else $t" | bc )
        calls=$(echo "if($calls > $c) $calls else $c" | bc )
    fi
    # Check totalProds
    if   (( $(echo "$totalProds > 2048^2" | bc) )); then
        errorMuchMemory "total number of products" "2048^2"
    fi
    # Check calls
    if   (( $(echo "$calls > 32768" | bc) )) && isOptWithoutArg "native"; then
        errorLongExecTime "exec=native and number of calls" "32768"
    elif (( $(echo "$calls > 32768" | bc) )) && isOptWithoutArg "opt_native" && isOptWithoutArg "0"; then
        errorLongExecTime "exec=opt_native, sol=0 and number of calls" "32768"
    elif (( $(echo "$calls > 262144" | bc) )) && isOptWithoutArg "opt_native"; then
        errorLongExecTime "exec=opt_native and number of calls" "262144"
    elif (( $(echo "$calls > 1048576" | bc) )); then
        errorLongExecTime "number of calls" "1048576"
    fi
fi

# -------------- Functions for the default action

# getLines <var_name> <file>
#
# Return the number of lines of a file
# It returns 1 if <file> does not exists
getLines() {
    local -n gLRef=$1 || wrongVarNameErr "$1"
    [[ -f $2 ]] || return 1
    gLRef=$(wc -l "$2")
    # %% is necessary in the case that the filename contains spaces
    gLRef=${gLRef%% *}
}

# getNFromFileNames <var_name> <time_file> <mem_file>
#
# Save to variable named <var_name> the N attribute specified in <time_file>
# and <mem_file>.
# Return 1 if files have a different N.
getNFromFileNames() {
    local -n gNFFNRef=$1 || wrongVarNameErr "$1"
    local timeN memN
    timeN=${2##*_N}; timeN=${timeN%%_*}
    memN=${3##*_N}; memN=${memN%%_*}
    [[ $timeN -eq $memN ]] || return 1
    gNFFNRef="$timeN"
}

# arePreviousResultsOK <timeFile> <memFile>
#
# Do some checks to have some assurance that reused results are not corrupted:
#   1. Both files must have the same number of lines.
#   2. Number of lines must be -le than the parameter N in the filename.
#   3. Structure of every line must be correct
#      (not done if debug variable is not defined !!!).
#
# Return 1 if at least one file is undoubtedly corrupted, it does not exist
# or filenames include different N parameters.
arePreviousResultsOK() {
    # Compatible number of lines
    local linesTime linesMem
    getLines linesTime "$1" || return 1
    getLines linesMem "$2" || return 1
    [[ $linesTime -eq $linesMem ]] || return 1

    # Valid number of lines
    local N
    getNFromFileNames N "$1" "$2" && [[ $linesMem -le $N ]] || return 1

    # The following test is really slow when the number of records is big.
    # Do it only if N is not too big.
    if [[ $N -le 20 ]]; then
        # Integrity of timeFile records
        while read -r field; do
            isReal "$field" || return 1
        done < "$1"

        # Integrity of memFile records
        while read -ra fields; do
            [[ ${#fields[@]} -eq 4 ]] || return 1
            local i
            for (( i=0; i<4; i++ )); do
                isReal "${fields[$i]}" || return 1
            done
        done < "$2"
    fi
}

# areResultsCompleted <timeFile> <memFile>
#
# Return 0 only if results do not seem corrupted and they have all the required
# records
areResultsCompleted() {
    local linesTime linesMem N
    getLines linesTime "$1"         || return 1
    getLines linesMem "$2"          || return 1
    getNFromFileNames N "$1" "$2"   || return 1
    [[ $linesTime -eq $N ]]         || return 1
    [[ $linesMem -eq $N ]]          || return 1
    arePreviousResultsOK "$1" "$2"
}

# getCmdAndString <cmd_var_name> <string_var_name> <exec>
getCmdAndString() {
    local -n gCASCmdRef=$1 || wrongVarNameErr "$1"
    local -n gCASStringRef=$2 || wrongVarNameErr "$2"
    case "$3" in
        JVM)        gCASCmdRef="$cmdJVM";       gCASStringRef="JVM"         ;;
        opt_native) gCASCmdRef="$cmdNativeOpt"; gCASStringRef="NativeOpt"   ;;
        native)     gCASCmdRef="$cmdNative";    gCASStringRef="Native"      ;;
        *)
            >&2 echo "Wrong executable type passed to ${FUNCNAME[0]}."
            exit 1 ;;
    esac
}

# getTestString <var_name> <test>
getTestString() {
    local -n gTSRef=$1 || wrongVarNameErr "$1"
    case "$2" in
        total)   gTSRef="ChangingTotalProducts" ;;
        average) gTSRef="ChangingCalls" ;;
        ratio)   gTSRef="ChangingProductsOrdersRatio" ;;
        *) >&2 echo "Wrong test passed to ${FUNCNAME[0]}."; exit 1 ;;
    esac
}

# getCommonOpts4Plot <var_name>
#
# It reads from the environment: Many options, maybe "x_max" through getxMax
getCommonOpts4Plot() {
    local -n gCO4PRef=$1 || wrongVarNameErr "$1"
    getArg N "-N"
    getArg c "-c"
    getArg o "-o"
    getArg p "-p"
    getArg t "-t"
    getArg resultsDir "-d"
    getArg graphsDir "-g"
    # Even if not all tests were done, it is not a problem to send unneeded
    # parameters. No warnings will be displayed when -q is present.
    gCO4PRef="-q -N $N -c $c -o $o -p $p -t $t -d $resultsDir -g $graphsDir"
}

# getFilenamesOfResults <time_file_var_name> <mem_file_var_name> \
#                       <test> <e> <N> <c> <o> <p> <s>
#
# It reads from the environment: Option "-d"
getFilenamesOfResults() {
    local -n gFORTimeFileRef=$1 || wrongVarNameErr "$1"
    local -n gFORMemFileRef=$2 || wrongVarNameErr "$2"
    local test="$3" e="$4" N="$5" c="$6" o="$7" p="$8" s="$9"
    local testString cmd cmdString resultsDir
    getTestString testString "$test"
    getCmdAndString cmd cmdString "$e"
    getArg resultsDir "-d"
    gFORTimeFileRef="${resultsDir}time_${cmdString}${testString}"
    gFORMemFileRef="${resultsDir}mem_${cmdString}${testString}"
    gFORTimeFileRef+="_N${N}_orders${o}_prods${p}_calls${c}_sol${s}.sal"
    gFORMemFileRef+="_N${N}_orders${o}_prods${p}_calls${c}_sol${s}.sal"
}

# exitTestCorrectly <test> <e> <s> <x> <timeFile> <memFile>
#
# Exit correctly when Ctr+C is pressed by the user.
exitTestCorrectly() {
    local msg="\\nExecution interrupted by SIGINT while performing a test."
    msg+="\\n\\nIncompleted results will be reused if possible.\\n"
    msg+="If you prefer to discard them, delete at least one. They are:\\n"
    >&2 echo -e "$msg"
    >&2 echo "$5"
    >&2 echo "$6"
    >&2 echo -e "\\nDon't panic! Please, wait while processing last results and generating graphs."

    # Remove predictable error generated by GNU time so results can be reused
    sed -i '/Command/,$d' "$6"

    # Calculate last x that was finished correctly (the one before current x).
    # If no x was finished (so x==1), variable gets value 0 that must be
    # managed correctly by functions called with this argument below.
    lastxFinishedCorrectly=$(($4/2))

    # Process results which were being generated (or skipped)
    processTestResults "$1" "$2" "$3" "$lastxFinishedCorrectly"
    plotWhatIsAvailable "$1" "$2" "$3" "$lastxFinishedCorrectly"
    >&2 echo "Done."
    exit 1
}

# runTest <test> <e> <s> <xMax>
#
# Run the test with the specified parameters
#
# It can read from the environment: options "-N", "-c", "-o", "-p", "-t".
#
# Return 1 if all results were reused.
runTest() {
    local test="$1" e="$2" s="$3" xMax="$4" testString cmd cmdString N x o p c
    getTestString testString "$test"
    getCmdAndString cmd cmdString "$e"
    getArg N "-N"
    local somethingWasDone=0
    for (( x=1; x<=xMax; x*=2 )); do
        case "$test" in
            total)
                getArg c "-c"
                o=$x p=$x ;;
            average)
                getArg o "-o"
                getArg p "-p"
                c=$x ;;
            ratio)
                getArg c "-c"
                getArg t "-t"
                ((o=x, p=t/o)) ;;
            *)
                >&2 echo "Internal error: Wrong test in ${FUNCNAME[0]}."
                exit 1 ;;
        esac

        getFilenamesOfResults timeFile memFile \
            "$test" "$e" "$N" "$c" "$o" "$p" "$s"

        # Manage a Ctr+C from a desperate user.
        trap "exitTestCorrectly $test $e $s $x $timeFile $memFile" INT

        # Decide if results comming from other executions of the program
        # must me deleted
        if [[ ! -f $timeFile || ! -f $memFile ]]; then
            # If at least one of the files do not exist, delete the other
            rm -f "$timeFile" "$memFile"
        elif ! arePreviousResultsOK "$timeFile" "$memFile"; then
            # If data is corrupted, display files content and remove them
            >&2 echo -e "Previous results are corrupted. They will be computed again.\\n"
            >&2 echo "LOG: Content of $timeFile was:"
            >&2 cat "$timeFile"
            >&2 echo "LOG: Content of $memFile was:"
            >&2 cat "$memFile"
            >&2 echo
            rm -f "$timeFile" "$memFile"
        fi

        echo -n "N=$N, test=$test, e=$e, c=$c, o=$o, p=$p, s=$s."

        # Skip iterations if previous results are reused
        local i=0
        if getLines i "$memFile"; then
            echo " Reusing $i."
        else
            echo
        fi
        [[ $i -ne $N ]] && somethingWasDone=1
        # Realizations loop
        for (( ; i<N; i++ )); do
            # Check if execution did not finish correctly.
            if command time -o "$memFile" -a -f "%M\\t%e\\t%U\\t%S" \
                    $cmd "$s" "$c" "$o" "$p" >> "$timeFile"; then
                local elapsedTime
                elapsedTime=$(tail -n 1 "$memFile" | cut -f2)
                echo "-> #$((i+1)) done in ${elapsedTime} s."
            else
                # Remove predictable garbage introduced by GNU time after
                # failing.
                sed -i '/Command/,$d' "$memFile"
                msg="Test failed! It usually happens when JVM runs out of "
                msg+="memory."
                errcho "$msg"
            fi
        done
    done
    (( somethingWasDone ))
}

# exitProcessingCorrectly <test> <e> <s> <last_x_completed>
#
# In case that user presses Ctr+C while processing, redo the processing to make
# sure everything is correct and create any valid graph
exitProcessingCorrectly() {
    local msg=" --> Execution interrupted by SIGINT while processing last results."
    msg+="\\nNo more tests will be done.\\n"
    msg+="Don't panic! Please, wait while re-processing the results and generating graphs."
    >&2 echo -e "$msg"

    processTestResults "$1" "$2" "$3" "$4"
    plotWhatIsAvailable "$1" "$2" "$3" "$4"
    >&2 echo "Done."
    exit 1
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Notes about SEM (Standard Error of the Mean) and sstdev (Sample Standard
# Deviation).
# In this program, N is the number of realizations (-N).
#
# meanM: sum = 0.; for(i=1..M) sum += x[i]; meanM = sum/M
#
# svarM: aux = 0.; for(i=1..M) aux += (x[i] - meanM)*(x[i] - meanM)
#         svarM = aux/(M-1)
#
# sstdevM = sqrt(svarM)
#
# semM = sstdevM/sqrt(M)
#
# NOTE: The following formulaes are not used in current version.
#
# If we have N mean's and sstdev's, each one obtained from M independent
# samples, we can calculate the statistics of the M*N samples as:
#
# meanMN: aux1 = 0.; for(i=1..N) aux1 += meanM; meanMN = aux1/N
#
# svarMN: aux2 = 0; for(i=1..N) aux2 += sstdevM; svarMN = aux2*(M-1)/(M*N-1)
#
# sstdevMN = sqrt(svar_MN)
#
# semMN = sstdevMN/sqrt(M*N) = sqrt(aux2*(M-1)/((M*N-1)*M*N))
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# processTestResults <test> <e> <s> <last_x_completed|0|-1>
#
# Process available results specified by <test>, <e> and <s> and options passed
# by user until x equal to <last_completed_x>. It also updates the excerpt DB.
# Fourth parameter equal to 0, is understood as no available results so nothing
# is done. Fourth parameter equal to -1 will process all available results.
#
# Error is thrown if fourth parameter is positive and some expected result is
# incomplete, corrupted or not present.
#
# It returns 1 if fourth parameter is -1 and there was no valid results for
# x=1. In that case, nothing is included in the excerpt database.
#
# N==1 will produce NaN for all SEMs.
#
# Code note: This function is not very elegant.
# I did not think of a better solution.
# At any case, this is not a bottle neck and most of the code is shared by
# different tests--I decided not to repeat code for each test so it is easier
# to maintain.
#
# It can read from the environment: options "-d", "-N", "-c", "-o", "-p", "-t"
processTestResults() {
    # Nothing to process if x has the flag value 0
    [[ $4 -eq 0 ]] && return 1
    trap "exitProcessingCorrectly $1 $2 $3 $4" INT

    local test="$1" e="$2" s="$3" lastx="$4" cmd cmdString N o p c t
    getCmdAndString cmd cmdString "$e"
    getArg N "-N"
    case "$test" in
        total)
            getArg c "-c"
            local filenameEnd="_N${N}_sol${s}_calls$c.sal"
            local headerBegin="# nOrders*nProducts" ;;
        average)
            getArg o "-o"
            getArg p "-p"
            local filenameEnd="_N${N}_sol${s}_orders${o}_prods$p.sal"
            local headerBegin="# nCalls" ;;
        ratio)
            getArg c "-c"
            getArg t "-t"
            local filenameEnd="_N${N}_sol${s}_calls${c}"
            filenameEnd+="_totalProds$t.sal"
            local headerBegin="# nOrders" ;;
        *)  >&2 echo "Internal error: Wrong test in ${FUNCNAME[0]}."; exit 1 ;;
    esac

    # Set filename an header of the file for processed results
    local testString resultsDir
    getTestString testString "$test"
    getArg resultsDir "-d"
    local filename="${resultsDir}results${cmdString}${testString}"
    filename+="${filenameEnd}"
    local header="${headerBegin}"
    header+="\\tmeanFunctExecTime(ms)\\tSEMFunctExecTime(ms)"
    header+="\\tMeanMemory(MB)\\tSEMMemory(MB)"
    header+="\\tmeanProgramRealTime(ms)\\tSEMProgramRealTime(ms)"
    header+="\\tmeanProgramUserTime(ms)\\tSEMProgramUserTime(ms)"
    header+="\\tmeanProgramSystemTime(ms)\\tSEMProgramSystemTime(ms)"
    echo -e "$header" > "$filename"

    local x
    for (( x=1; lastx==-1 || x<=lastx; x*=2 )); do
        case "$test" in
            total)
                o=$x p=$x
                local awk="awk -v o=$o -v N=$N -v c=$c"
                local awkProgBegin='BEGIN{OFS="\t"} {print o^2, $1, ' ;;
            average)
                c=$x
                local awk="awk -v c=$c -v N=$N"
                local awkProgBegin='BEGIN{OFS="\t"} {print c, $1, ' ;;
            ratio)
                ((o=x, p=t/o))
                local awk="awk -v o=$o -v N=$N -v c=$c"
                local awkProgBegin='BEGIN{OFS="\t"} {print o, $1, ' ;;
            *)
                >&2 echo "Wrong test in ${FUNCNAME[0]}."; exit 1 ;;
        esac

        # Determine whether results for current parameters are enough to be
        # processed.
        local timeFile memFile
        getFilenamesOfResults timeFile memFile \
            "$test" "$e" "$N" "$c" "$o" "$p" "$s"
        if ! areResultsCompleted "$timeFile" "$memFile"; then
            # Simpler idea I have is to delete non-processed file after
            # creation if nothing was added.
            [[ $x -eq 1 ]] && rm "$filename"
            # Results for any x bigger than current were not computed if
            # execution enters here.
            # That should not happen if provided lastx is different to -1.
            msg="No valid results for N=$N, test=$test, e=$e, c=$c, "
            msg+="o=$o, p=$p, s=$s."
            [[ $lastx -ne -1 ]] && errcho "$msg"
            # Nothing to include in DB if x==1 does not have results
            [[ $x -eq 1 ]] && return 1
            break
        fi

        #-------------------------------------------------------------------
        # Fields processed by awk:
        # (Notation from the math explanation above):
        #
        #   functTime_meanN(1) functTime_sstdevN(2)
        #   memory_meanN(3) memory_sstdevN(4)
        #   elapsedTime_meanN(5) elapsedTime_sstdevN(6)
        #   userTime_meanN(7) userTime_sstdevN(8)
        #   systemTime_meanN(9) systemTime_sstdevN(10)
        #-------------------------------------------------------------------

        # awk (/1000 means KB->MB, *1000 means s->ms)
        local awkProgEnd='$2/sqrt(N), '
        awkProgEnd+='$3/1000., ($4/1000.)/sqrt(N), '
        awkProgEnd+='$5*1000, ($6*1000)/sqrt(N), $7*1000, '
        awkProgEnd+='($8*1000)/sqrt(N), $9*1000, '
        awkProgEnd+='($10*1000)/sqrt(N)}'

        local awkProg="${awkProgBegin}${awkProgEnd}"
        local datamash="datamash mean 1 sstdev 1 mean 2 sstdev 2 "
        datamash+="mean 3 sstdev 3 mean 4 sstdev 4 mean 5 sstdev 5"
        
        paste "$timeFile" "$memFile" | LANG=EN $datamash \
            | $awk "$awkProg" >> "$filename"
    done

    # Send last completed x, if loop finishes correctly or not
    case "$test" in
        total) totalTestUpdate "$N" "$e" "$c" "$s" "$((x/2))" ;;
        average) averageTestUpdate "$N" "$e" "$o" "$p" "$s" "$((x/2))" ;;
        ratio) ratioTestUpdate "$N" "$e" "$c" "$s" "$t" "$((x/2))" ;;
        *) >&2 echo "Internal error: Wrong test in ${FUNCNAME[0]}."; exit 1 ;;
    esac
}

# plotWhatIsAvailable <last_test> <last_e> <last_s> <last_x_completed|0>
#
# Plot all available results.
#
# It reads from environment: sols, execs, tests and options through
#                            getCommonOpts4Plot
plotWhatIsAvailable() {
    local commonOpts
    getCommonOpts4Plot commonOpts
    # Multi-loop in the same order than the main loop of the program
    for test in "${tests[@]}"; do
        local execsBeforeLast=()
        for e in "${execs[@]}"; do
            local solsWithSomeData=()
            # For last exec, include last solution only if some result was
            # obtained ($4 -ne 0)
            for s in "${sols[@]}"; do
                [[ $1 = "$test" && $2 = "$e" && $3 = "$s" && $4 -eq 0 ]] && break
                solsWithSomeData+=("$s")
                [[ $1 = "$test" && $2 = "$e" && $3 = "$s" ]] && break
            done
            [[ $1 = "$test" && $2 = "$e" ]] && break
            execsBeforeLast+=("$e")
        done
        if [[ ${#execsBeforeLast[@]} -eq ${#execs[@]} ]]; then
            # If I arrived here without a break, plot all for -x and -m
            "$plotScript" $commonOpts -m "$test" "${execs[@]}" "${sols[@]}"
        else
            # Else, plot what is possible for -x and -m:
            #  * For every explored exec: sols shared by any exec
            #  * For execs before last one: the other sols

            local execsStarted=("${execsBeforeLast[@]}" "$e") # At least size=1
            if [[ ${#execsStarted[@]} -gt 0 && ${#solsWithSomeData[@]} -gt 0 ]]; then
                "$plotScript" $commonOpts -m "$1" "${execsStarted[@]}" "${solsWithSomeData[@]}"
            fi
            # Note: This is a string, not an array. Not a problem since sols are single numbers
            local solsWithNoDataInLastExec=${sols[@]:${#solsWithSomeData[@]}} # Maybe ==""
            if [[ ${#execsBeforeLast[@]} -gt 0 && -n ${solsWithNoDataInLastExec:+x} ]]; then
                "$plotScript" $commonOpts -m "$1" "${execsBeforeLast[@]}" ${solsWithNoDataInLastExec[*]}
            fi
        fi
        [[ $1 = "$test" ]] && break
    done
}

# ------------- Do the real stuff -----

if ! datamash --version >& /dev/null; then
    errcho "datamash command not found."
fi

# Compile if needed
if isOptWithoutArg "default_action"; then
    trap 'errcho "Execution finished by SIGINT during compilation."' INT
    # Generate the jarfile
    if isOptWithoutArg "JVM"; then
        # Check java installation
        if ! "$javaBin" --version >& /dev/null; then
            errcho "Error: java command not found." \
                "Please, hardcode correct path in javaBin variable (line 6)."
        fi
        # Generate jarfile
        if [ -f "$jarFile" ]; then
            echo "--> Using previously generated jarfile."
        else
            echo "--> Creating a jarfile from Kotlin code..."
            if ! "$kotlincBin" -version >& /dev/null; then
                errcho "Error: kotlinc command not found." \
                    "Please, hardcode correct path in kotlincBin variable (line 4)."
            fi
            "$kotlincBin" "$testCode" -include-runtime -d "$jarFile"
        fi
    fi
    # Compile the optimized native executable
    if isOptWithoutArg "opt_native"; then
        if [ -f "$nativeBinOpt" ]; then
            echo "--> Using previously generated optimized native executable."
        else
            # Check kotlinc binary
            if ! "$kotlincNativeBin" -version >& /dev/null; then
                errcho "Error: kotlinc-native command not found." \
                    "Please, hardcode correct path in kotlincNativeBin variable (line 5)."
            fi
            # Compilation
            echo "--> Compiling Kotlin code to optimized native executable"
            "$kotlincNativeBin" "$testCode" -o "$nativeBinOpt" -opt
        fi
    fi
    # Compile the native executable
    if isOptWithoutArg "native"; then
        if [ -f "$nativeBin" ]; then
            echo "--> Using previously generated native executable."
        else
            # Check kotlinc binary
            if ! "$kotlincNativeBin" -version >& /dev/null; then
                errcho "Error: kotlinc-native command not found." \
                    "Please, hardcode correct path in kotlincNativeBin variable (line 5)."
            fi
            # Compilation
            echo "--> Compiling Kotlin code to a native executable..."
            "$kotlincNativeBin" "$testCode" -o "$nativeBin"
        fi
    fi
fi
# Probably it is not trapped ever since other traps are defined later,
# but I feel safer informing correctly
trap 'errcho "Execution stopped by SIGINT."' INT

# ------------------------------ Main loop -----------------------------
isOptWithoutArg "-P" && subject="processing" || subject="tests"
for test in "${tests[@]}"; do
    # Run the test for all requested parameters
    getxMax xMax "$test"
    echo -e "--> Starting \"$test\" $subject."
    for e in "${execs[@]}"; do
        for s in "${sols[@]}"; do
            if isOptWithoutArg "-P"; then
                isOptWithoutArg "x_max" && lastx=$xMax || lastx="-1"
                if processTestResults "$test" "$e" "$s" "$lastx"; then
                    somethingWasDone=1
                else
                    >&2 echo "Warning: Nothing processed for test=$test, e=$e, s=$s."
                fi
            else
                # Process results only if something new was calculated
                if runTest "$test" "$e" "$s" "$xMax"; then
                    echo "--> Processing generated results..."
                    processTestResults "$test" "$e" "$s" "$xMax"
                fi
            fi
        done
    done
done
if isOptWithoutArg "-P" && ! (( somethingWasDone )); then
    getArg resultsDir "-d"
    (( DBCreatedInThisExecution )) && rm "${resultsDir}${excerptDBname}"
    errcho "No compatible result was found in directory: \"$resultsDir\"."
fi
if isOptWithoutArg "default_action"; then
    # Create graphs at the end
    echo -e "\\n--> Generating merged plots..."
    getCommonOpts4Plot commonOpts
    "$plotScript" $commonOpts -m "${tests[@]}" "${execs[@]}" "${sols[@]}"
fi
