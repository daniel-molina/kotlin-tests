# NDimArray.gnu
#
# This is free and unencumbered software released into the public domain.
# For more information, please refer to <https://unlicense.org>
#------------------------------------------------------------------------------
# Motivation:
#   * By the moment, gnuplot only supports 1D arrays. A multidimensional is
#     very useful some times even if it is not very efficient.
#
# Characteristics and limitations:
#   * This library simulates a multidemensional array up to 11 dims (the
#     maximum number of function arguments for a user-defined function is 12).
#   * If you need more dimensions, code should be easily modifiable to pass the
#     name of an array which includes the indices instead of one index per
#     argument. If you need exactly 12 dimensions, you can comment function
#     set_name and use the alternative function described below.
#   * A 1D gnuplot array is used to store the data.
#   * Contiguous indices in the data array correspond with the rightmost
#     indices of the N-dim array.
#   * It is possible to use contiguous integers and strings as indices.
#   * If strings are used, current implementation needs to loop over the list
#     of valid strings until passed index is found when the array is accessed.
#
# Usage: call "NDimArray.gnu" name sizes [indices_names]
#
# Arguments:
#   name: A unique name that will be part of provided functions and variables
#         internally used. Read section 'Operations' and 'Created/Modified
#         variables' to be sure that provided name will make this library not
#         collide with your own variables.
#
#   sizes:  A string containing the sizes of each dimension of the array
#           separated by spaces. The number of contained words is, as a
#           consequence, the number of dimensions of the array. Only arrays
#           between 1 and 11 dimensions are allowed.
#
#           If indices_names (see below) is not passed, letter 'i' can
#           optionally be included after any size. Independently of that, each
#           dimension will by accessed by consecutive integers starting from 1.
#           Example, sizes being "3 4" or "3i 4i" when indices_names is not
#           provided means that we require a 2D array where the 1st dimension
#           has size 3 and the 2nd one has size 4, accessed by a first index
#           from 1 to 3 and a 2nd index from 1 to 4.
#
#           If indices_names is provided, those sizes corresponding to
#           dimensions indexed by consecutive integers must include
#           letter 'i' after the size. If 'i' is not included for a given
#           dimension, it will be considered as a dimension indexed by strings
#           (see below).
#
#   indices_names: If you want certain indices to start from a value different
#                  than 1 (e.g. 0) or to pass certain strings instead of
#                  integers, use this optional parameter.
#
#                  indices_names must be a string containing words separated by
#                  spaces. It is read from left to right and understood
#                  according to the following rules:
#
#                  1a. If the first word in sizes finishes at 'i', the first
#                      element of indices_names must be the first integer index
#                      that you want to use for the first dimension. It is
#                      mandatory to specify it even if you want the index to
#                      start from 1.
#
#                  1b. If the first word in sizes does not finish at 'i', 
#                      include (separated by spaces) the same number of words
#                      as the first value of sizes indicates. Each word will be
#                      a valid string to access the first dimension of the
#                      array.
#
#                  2. If sizes contains a second word, repeat step 1a or 1b
#                     whether 'i' is present at the end of that word, appending
#                     the new initial integer index or set of strings,
#                     separated by a space from the last word in indices_names.
#
#                  3. If there are more words in sizes, repeat step 2 by
#                     advancing one word each time until there are none left.
#
#                  Example 1: sizes="3i 2 5i", indices_names="0 foo bar -3"
#
#                  Example 2: sizes="4 13i", indices_names="A B C D 1"
#
# Main functions:
#
#   For simplicity, for the examples below suppose that this library was called
#   as:
#
#     call "scripts/NDimArray.gnu" "arr" "2i 3 4i" "1 A B C 0"
#
#   These common errors can happen when using the functions described below:
#
#     * Passing an index out of the valid range.
#     * Passing an integer index when a string index should be used instead
#       (passing string indices instead of integers is OK, e.g., '9' instead of
#       9).
#     * Passing (unintentionally) a string index without quoting. e.g. A
#       instead of "A" or 'A'. A would be interpreted as variable, not a
#       string.
#     * Accessing an unitialized value of the array.
#
# 1. Inserting elements:
#
#   * Long and explicit way:
#
#           data_name[flatInd_name(i1, ..., iN)] = value
#
#     Examples:
#
#           data_arr[flatInd_arr(1, 'B', 3)] = "cat"
#           data_arr[flatInd_arr(1, 'A', 4)] = 3 # Error: array out of range
#           my_int_index = 1
#           my_string_index = "A"
#           data_arr[flatInd_arr(my_int_index, my_string_index, 3)] = "cat"
#
#   * Shorter, but inserted value is returned, so you have to do something
#     with it (for example, assigning it to an unneeded variable):
#
#           set_name(i1, ..., iN, value)
#
#     Examples:
#
#           v = set_arr(1, 'B', 0, 9)    # v is a variable that you do not need
#           w = set_arr(2, 'D', 2, "ds") # Error: array out of range
#
# 2. Getting elements previously inserted:
#
#           get_name(i1, ..., iN)
#
#    Examples:
#
#           g = get_arr(2, 'C', 2)
#           var = get_arr(2, 5, 2) # Error: passed number instead of string
#
#    Note: You will obtain an undefined variable if a value was not introduced
#          previously for passed indices to name_get.
#
# Utilites:
#
#  * The following variables and functions finished at "_name" are created or
#    overwritten, where name is the first argument passed ("arr" in the
#    examples above). Some of them are needed by other functions to work,
#    included the main functions described above. Better do not collide with
#    them, but the use can find useful to call/read them.
#
#       - sizes_name: A copy of sizes (needed by isDimMarkedWithi_NDA
#                     and isIndexInteger_name, but they all are not needed for
#                     the main functions)
#
#       - Ndims_name: Number of dimensions of the array.
#
#       - isDimMarkedWithi_name(dim): A function that returns 0 if 'i' letter
#                                     is not found in the size specifier for 
#                                     dimension dim. Else, it returns a
#                                     positive number. If dim is not in the
#                                     range 1:Ndims_name, it silently returns
#                                     -1 (needed by isIndexInteger_name).
#
#       - isIndexInteger_name(dim): Identical to the previous one, except that
#                                   it returns 1 whenever no indices_names is
#                                   provided and dim is a valid dimension.
#
#       - Nelems_name: number of elements of the array.
#
#       - data_name: The 1D array in which the array data is stored
#                    (necessary).
#
#       - normalizedInd_*_arr(i): There are N of them. Each one normalizes
#                                   one of the N-dim indices from 0 to the size
#                                   of the dimension - 1. The ordinal of the
#                                   dimension (from 1 to N) substitutes * in
#                                   each of them. If i is out od range, it
#                                   silently returns Nelems_name (necessary).
#
#       - flatInd_arr(i1, ..., iN): Function mapping all N-dim indices to a
#                                   valid index for data_name (from 1 to
#                                   Nelems_name). If any index is out of
#                                   range, it returns a value bigger than
#                                   Nelems_name (necessary).
#
#   In addition, if indices_names is provided:
#
#       - indicesDetails_name: A copy of indices_name (necessary).
#       - indexName2FlatIndex_name(name, start, end): A function that returns
#                                                     the position of name
#                                                     (from 0 to end - start)
#                                                     in indicesDetails_name
#                                                     starting the search from
#                                                     start and ending at end.
#                                                     If name is not found, it
#                                                     returns Nelems_name. If
#                                                     name is not a string, it
#                                                     throws an error. If start
#                                                     or end are not valid
#                                                     positions, it silently
#                                                     returns an unexpected
#                                                     value (necessary).
#
#
# Read variables: None.
#
# Unuseful created/modified variables:
#
#  *_NDA: Some variables finished at "_NDA" (literally) are created or
#         overwritten at any call. After the call they are not necessary at
#         all.
#
#               - array sizes_NDA
#               - rawSize_NDA
#               - namei_NDA
#               - indices_NDA
#               - factor_NDA
#               - flatInd_NDA
#
#          In addition, if indices_names is provided:
#
#               - x_NDA
#------------------------------------------------------------------------------

if (ARGC < 2 || ARGC > 3) {
    print "NDimArray.gnu: Error: Wrong number of arguments."
    print 'NDimArray.gnu: Usage: call "NDimArray.gnu" name sizes [indices_names]'
    exit
}

Ndims_@ARG1 = words(ARG2)
if (Ndims_@ARG1 < 1 || Ndims_@ARG1 > 11) {
    print "NDimArray.gnu: Error: sizes must contain between 1 and 12 words."
    print 'NDimArray.gnu: Usage: call "NDimArray.gnu" name sizes [indices_names]'
    exit
}

# Some utilities, internally used or not
sizes_@ARG1 = ARG2
isDimMarkedWithi_@ARG1(dim) = dim < 1 || dim > Ndims_@ARG1 ? -1 \
    : strstrt(word(sizes_@ARG1, dim), "i")
if (ARGC > 2) {
    indicesDetails_@ARG1 = ARG3
    isIndexInteger_@ARG1(dim) = isDimMarkedWithi_@ARG1(dim)
} else {
    isIndexInteger_@ARG1(dim) = dim < 1 || dim > Ndims_@ARG1 ? -1 : 1
}

# Save sizes from ARG2, removing 'i' from the end of the number if needed.
# That avoids the check and possibly removal of 'i' every time.
array sizes_NDA[Ndims_@ARG1]
do for [dim_NDA = 1:Ndims_@ARG1] {
    sizes_NDA[dim_NDA] = strstrt(rawSize_NDA = word(ARG2, dim_NDA), "i") \
        ? rawSize_NDA[1:strlen(rawSize_NDA)-1] : rawSize_NDA
}

Nelems_@ARG1 = 1
do for [dim_NDA = 1:Ndims_@ARG1] {
    Nelems_@ARG1 = Nelems_@ARG1 * sizes_NDA[dim_NDA]
}
array data_@ARG1[Nelems_@ARG1]

if (ARGC >= 3) {
    # indexName2FlatIndex_ARG1(name, index_start, index_end)
    #
    # It returns the relative position of name in indexNames_@ARG1 from
    # index_start if its absoulute position is in the range
    # index_start:index_end (both limits included).
    # Else, it returns Nelems_@ARG1.
    eval "indexName2FlatIndex_".ARG1."(name, start, end)" \
        ." = end < start ? ".Nelems_@ARG1 \
        ." : ( name eq word(indicesDetails_".ARG1.", end)" \
        ." ? end - start : indexName2FlatIndex_".ARG1."(name, start, end-1) )"
}

# normalizedInd_dim_ARG1 <index|index_name>
# It returns a unique non-negative value for every different valid index or
# index_name. Range of values goes from 0 to sizes_NDA[dim] - 1.
# If index or index_name are not valid, it returns Nelems_@ARG1.
namei_NDA = 1
do for [dim_NDA = 1:Ndims_@ARG1] {
    if (ARGC < 3) {
        # This case supposes that indices are in [1, sizes_NDA[dim_NDA]]
        eval "normalizedInd_".dim_NDA."_".ARG1."(i)" \
            ." = i > 0 && i <= ".sizes_NDA[dim_NDA]." ? i - 1 : ".Nelems_@ARG1
    } else {
        if (isDimMarkedWithi_@ARG1(dim_NDA)) {
            if (words(indicesDetails_@ARG1) < namei_NDA) {
                print "NDimArray.gnu: Error: Not enough words in index_names."
                print 'NDimArray.gnu: Usage: call "NDimArray.gnu" name sizes [indices_names]'
                exit
            }
            # This case supposes that indices are in
            # [x, x - 1 + sizes_NDA[dim_NDA]] where x is provided by ARG3.
            # Mind safety note: Used < instead of <=, so -1 was not written.
            x_NDA = word(indicesDetails_@ARG1, namei_NDA)
            eval "normalizedInd_".dim_NDA."_".ARG1."(i)" \
                ." = i >= ".x_NDA." && i < ".x_NDA." + ".sizes_NDA[dim_NDA] \
                ." ? i - ".x_NDA." : ".Nelems_@ARG1
            namei_NDA = namei_NDA + 1
        } else {
            if (words(indicesDetails_@ARG1) < namei_NDA + sizes_NDA[dim_NDA] - 1) {
                print "NDimArray.gnu: Error: Not enough words in index_names."
                print 'NDimArray.gnu: Usage: call "NDimArray.gnu" name sizes [indices_names]'
                exit
            }
            eval "normalizedInd_".dim_NDA."_".ARG1."(i)" \
                ." = indexName2FlatIndex_".ARG1 \
                ."(i, ".namei_NDA.", ".(namei_NDA + sizes_NDA[dim_NDA] - 1).")"
            namei_NDA = namei_NDA + sizes_NDA[dim_NDA]
        }
    }
}
if (ARGC >= 3 && words(indicesDetails_@ARG1) >= namei_NDA) {
    print "NDimArray.gnu: Error: Too many words in index_names."
    print 'NDimArray.gnu: Usage: call "NDimArray.gnu" name sizes [indices_names]'
    exit
}

indices_NDA = "i1"
do for [dim_NDA = 2:Ndims_@ARG1] {
    indices_NDA = indices_NDA.", i".dim_NDA
}

# flatIndex_ARG1(i1, i2, ..., iN)
#
# Computes the associated 1D-index according to passed indices.
#
# Idea of the implementation:
#
# * We are going to provide a "flat index" from 1 to Nelems_@ARG1. That way,
#   it will be possible to map:
#
#      A(i1, ..., iN) --> data_A[ flatInd(i1, ..., iN) ].
#
# * As an intermediate step we will obtain normalized indices I1, I2, ..., IN
#   starting at 0 for simplicity. Then, flatInd can be easily obtained using
#   the sizes of each dimension N1, N2, ..., NN.
#
#      3D case: flatInd(I1, I2, I3) = 1 + I3 + N3*I2 + N3*N2*I1.
#
# If some index passed by user is wrong, normalizedInd will be Nelems_@ARG1,
# so the flat index will be always greater than it. Then, any to the array
# will result in "array index out of range".
#
factor_NDA = 1
flatInd_NDA = "1 + normalizedInd_".|sizes_NDA|."_".ARG1 \
    ."(i".|sizes_NDA|.")"
do for [dim_NDA = Ndims_@ARG1 - 1:1:-1] {
    factor_NDA = factor_NDA*sizes_NDA[dim_NDA + 1]
    flatInd_NDA = flatInd_NDA." + ".factor_NDA."*" \
        ."normalizedInd_".dim_NDA."_".ARG1."(i".dim_NDA.")"
}
eval "flatInd_@ARG1(@indices_NDA) = ".flatInd_NDA

# ARG1_get(i1, i2, ..., iN)
get_@ARG1(@indices_NDA) = data_@ARG1[flatInd_@ARG1(@indices_NDA)]

# ARG1_set(i1, ..., iN, value)
set_@ARG1(@indices_NDA, v) = (data_@ARG1[flatInd_@ARG1(@indices_NDA)] = v)
