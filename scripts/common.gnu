# --------- Common settings --------

solNames = "usingFlatMap copyingMax2GrowingList " \
    ."copyingMax2InitializedList accumulator accumulatorFold " \
    ."usingFlatMapWithSeq"

if (kotlinExecs eq "JVM NativeOpt Native") {
    kotlinExecsFormatted = "all"
} else {
    kotlinExecsFormatted = system("echo ".kotlinExecs."|sed 's/\\s\\+/+/g'")
}

if (sols eq "0 1 2 3 4 5") {
    solsFormatted = "all"
} else {
    solsFormatted = system("echo ".sols."|sed 's/\\s\\+/+/g'")
}

# A 2D array to store the number of points to skip (-1 if all of them)
call "scripts/NDimArray.gnu" "skipNPoints" \
    "".words(sols)." ".words(kotlinExecs) \
    sols." ".kotlinExecs
# Return y only if row passed condition established for i and e in previous
# call to skipPoints.gnu
filterPoints(i, e, row, y) = row >= abs(get_skipNPoints(i, e)) ? y : NaN

inFilesBeginning = inDir."results"
outFilesMiddle = "_".kotlinExecsFormatted."_".solsFormatted."_"

system 'mkdir -p "'.outDir.'"'

# ---------- Consistency checks -----------------

do for [w in kotlinExecs] {
    if (w ne "JVM" && w ne "Native" && w ne "NativeOpt") {
        print "Error: Wrong word in kotlinExecs: ", w, "."
        exit
    }
}

do for [w in sols] {
    if (w ne "0" && w ne "1" && w ne "2" && w ne "3" && w ne "4" \
            && w ne "5") {
        print "Error: Wrong word in sols: ", w, "."
        exit
    }
}

do for [w in plots] {
    if (w ne "fun" && w ne "mem" && w ne "prog" && w ne "CPU") {
        print "Error: Wrong word in plots: ", w, "."
        exit
    }
}

if ((exists("yMin") && ! exists("yMax")) \
    || (! exists("yMin") && exists("yMax"))) {
    print "Error: yMin and yMax must be provided together."
    exit
}

if (exists("yMin") && yMin == yMax) {
    print "Error: yMin and yMax must be different " \
        ."(passed values are ", yMin, " and ", yMax, ")."
    exit
}

if ((exists("xKey") && ! exists("yKey")) \
    || (! exists("xKey") && exists("yKey"))) {
    print "Error: xKey and yKey must be provided together."
    exit
}

if (exists("yMin") && words(plots) > 1) {
    if (plotRelativeValues) {
        print "Warning: yMin and yMax have been set for more than one plot at a time."
    } else {
        print "Error: yMin and yMax can only be used for one plot at a time."
        exit
    }
}

if (exists("xKey") && words(plots) > 1) {
    print "Warning: xKey and yKey have been set for more than one plot at a time."
}

if (plotErrorLines && strstrt(plots, "CPU")) {
    print "Warning: Current version does not plot CPU graphs with error lines."
}

# If necessary, report missing files and exit
fileDoesNotExist(fileName) \
    = system("[ ! -f '".fileName."' ] && echo 1 || echo 0")
do for [e in kotlinExecs] {
    do for [i in sols] {
        if (fileDoesNotExist(inFilesBeginning.e.inFilesMiddle.i.inFilesEnding)) {
            if (! exists("noInFile")) {
                print "Error: Results not available for the following test(s):"
            }
            noInFile = 1
            brief = " test=".test.", exec=".e.", ".briefMiddle \
                .", sol=".i." (".word(solNames, i+1).")."
            print brief
        }
    }
}
if (exists("noInFile")) {
    print "No graph has been generated for them.\n"
    exit
}

#-------------------- Plots -----------------------

sizeWidth = 3400
sizeHeight = 1920
fontSettings = "Helvetica,".fontSize

keySpacing = 1.3
keyHeight = 1.2

if (plotErrorLines) {
    unset errorbars
    pointsSize = 0
    lineWidth = 5
} else {
    pointsSize = 7
    lineWidth = 7
}
ptJVM = 7
ptNative = 9
ptNativeOpt = 5

# In principle, different sols have different colors
# and differents execs different point types.
# If only plotting, one solution at a time, use different colors also for
# different execs to improve appearance
ltJVM(i)       = words(sols) != 1 ? i : 1
ltNative(i)    = words(sols) != 1 ? i : 2
ltNativeOpt(i) = words(sols) != 1 ? i : 3

set for [i = 1:6] style line i    lw lineWidth lt ltJVM(i) \
    ps pointsSize pt ptJVM
set for [i = 1:6] style line i+6  lw lineWidth lt ltNative(i) \
    ps pointsSize pt ptNative
set for [i = 1:6] style line i+12 lw lineWidth lt ltNativeOpt(i) \
    ps pointsSize pt ptNativeOpt

fls(i, e) = e eq "JVM" ? i+1 : ( e eq "Native" ? i+7 : i+13 )

set log x

if (strstrt(plots, "fun") && ! plotRelativeValues) {

    set yl "Function execution time"
    set ytics auto

    if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
        undefine yMin yMax
    }
    call "scripts/setYRange.gnu" \
        "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" "u 2" \
        'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]'

    # Force logscale or linear scale if asked
    # Modify also variable isYLog created by setYRange
    if (exists("yLogscale")) {
        if (yLogscale == 1) {
            set log y
            isYLog = 1
        } else {
            unset log y
            isYLog = 0
        }
    }

    if (yMinReal >= 1000) {
        plotInSeconds = 1
        set format y "%g s"
        # In the case yMin and yMax were used in setYRange,
        # we want to re-set the range to seconds.
        if (is_yMin_yMax_read) {
            set yr [yMin/1000.:yMax/1000.]
        }
    } else {
        plotInSeconds = 0
        set format y "%g ms"
        if (isYLog) {
            set ytics add ("1 s" 1e3, "10 s" 1e4, "100 s" 1e5, \
                "1000 s" 1e6, "10000 s" 1e7, "100000 s" 1e8)
        }
    }

    if (exists("xKey") && exists("yKey")) {
        set key at xKey, (plotInSeconds ? yKey/1000. : yKey) \
            sp keySpacing h keyHeight
    } else {
        set key top left sp keySpacing h keyHeight
    }

    set term png font fontSettings size sizeWidth,sizeHeight
    set output outDir."graphFunctExecTime".outFilesMiddle.outFilesEnding

    if (plotErrorLines) {
        plot for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:(plotInSeconds ? $2/1000. : $2) w lp ls fls(i, e) \
            t e." ".word(solNames, i+1), \
            for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:(plotInSeconds ? $2/1000. : $2):(plotInSeconds ? $3/1000. : $3) \
            w ye ls fls(i, e) not
    } else {
        plot for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:(plotInSeconds ? $2/1000. : $2) w lp ls fls(i, e) \
            t e." ".word(solNames, i+1)
    }
}

if (strstrt(plots, "mem") && ! plotRelativeValues) {

    set yl "Maximum resident set size"
    set ytics auto

    if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
        undefine yMin yMax
    }
    call "scripts/setYRange.gnu" \
        "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" "u 4" \
        'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]'

    # Force logscale or linear scale if asked
    # Modify also variable isYLog created by setYRange
    if (exists("yLogscale")) {
        if (yLogscale == 1) {
            set log y
            isYLog = 1
        } else {
            unset log y
            isYLog = 0
        }
    }

    set format y "%g MB"

    if (exists("xKey") && exists("yKey")) {
        set key at xKey, yKey sp keySpacing h keyHeight
    } else {
        set key top left sp keySpacing h keyHeight
    }

    set term png font fontSettings size sizeWidth,sizeHeight
    set output outDir."graphMem".outFilesMiddle.outFilesEnding

    if (plotErrorLines) {
        plot for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:4 w lp ls fls(i, e) t e." ".word(solNames, i+1), \
            for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:4:5 w ye ls fls(i, e) not

    } else {
        plot for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:4 w lp ls fls(i, e) t e." ".word(solNames, i+1)
    }
}

if (strstrt(plots, "prog") && ! plotRelativeValues) {

    graphName=outDir."graphProgramExecTime".outFilesMiddle.outFilesEnding

    set yl "Program execution time (System+User)"
    set ytics auto

    # Precision of GNU time is 10 ms.
    # Skip times that probably have not been correctly averaged.
    call "scripts/skipPoints.gnu" \
        "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
        "$8+$10>30" "data_skipNPoints[flatInd_skipNPoints(i, e)]" \
        "for [e in kotlinExecs] for [i in sols]"

    # Manage if some or all files must be excluded
    if (! someDataConsidered) {
        print "WARNING: Following graph was not created (program execution times below 30 ms):"
        print graphName
    } else {
        if (someDataSkipped) {
            print "Note: Program execution times below ~30ms not included."
        }
        if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
            undefine yMin yMax
        }
        call "scripts/setYRange.gnu" \
            "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
            "u (filterPoints(i, e, $0, $8+$10))" \
            'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]' \
            'get_skipNPoints(i, e) >= 0'

        # Force logscale or linear scale if asked
        # Modify also variable isYLog created by setYRange
        if (exists("yLogscale")) {
            if (yLogscale == 1) {
                set log y
                isYLog = 1
            } else {
                unset log y
                isYLog = 0
            }
        }

        if (yMinReal >= 1000) {
            plotInSeconds = 1
            set format y "%g s"
            # In the case yMin and yMax were used in setYRange,
            # we want to re-set the range to seconds.
            if (is_yMin_yMax_read) {
                set yr [yMin/1000.:yMax/1000.]
            }
        } else {
            plotInSeconds = 0
            set format y "%g ms"
            if (isYLog) {
                set ytics add ("1 s" 1e3, "10 s" 1e4, "100 s" 1e5, \
                    "1000 s" 1e6, "10000 s" 1e7, "100000 s" 1e8)
            }
        }

        if (exists("xKey") && exists("yKey")) {
            set key at xKey, (plotInSeconds ? yKey/1000. : yKey) \
                sp keySpacing h keyHeight
        } else {
            set key top left sp keySpacing h keyHeight
        }

        set term png font fontSettings size sizeWidth,sizeHeight
        set output graphName

        # var(X+Y)=var(X)+var(Y) => SEM(X+Y)=sqrt(SEM^2(X)+SEM^2(Y))
        sem(x,y)=sqrt(x**2+y**2)
        if (plotErrorLines) {
            plot for [e in kotlinExecs] for [i in sols] \
                inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
                u 1:(filterPoints(i, e, $0, \
                    plotInSeconds ? ($8+$10)/1000. : $8+$10)) \
                w lp ls fls(i, e) t e." ".word(solNames, i+1), \
                for [e in kotlinExecs] for [i in sols] \
                inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
                u 1:(filterPoints(i, e, $0, \
                    plotInSeconds ? ($8+$10)/1000. : $8+$10)) \
                    :(filterPoints(i, e, $0, \
                    plotInSeconds ? sem($9,$11)/1000. :sem($9,$11))) \
                w ye ls fls(i, e) not
        } else {
            plot for [e in kotlinExecs] for [i in sols] \
                inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
                u 1:(filterPoints(i, e, $0, \
                    plotInSeconds ? ($8+$10)/1000. : $8+$10)) \
                w lp ls fls(i, e) t e." ".word(solNames, i+1)
        }
    }
}

if (strstrt(plots, "CPU") && ! plotRelativeValues) {

    graphName=outDir."graphCPU".outFilesMiddle.outFilesEnding

    set yl "Percentage of CPU used by the entire program"
    set ytics auto

    # Precision of GNU time is 10 ms.
    # Skip times that probably have not been correctly averaged.
    call "scripts/skipPoints.gnu" \
        "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
        "$8+$10>30 && $6>30" "data_skipNPoints[flatInd_skipNPoints(i, e)]" \
        "for [e in kotlinExecs] for [i in sols]"

    # Manage if some or all files must be excluded
    if (! someDataConsidered) {
        print "WARNING: Following graph was not created (prog. execution times below 30 ms):"
        print graphName
    } else {
        if (someDataSkipped) {
            print "Note: CPU percentage values for prog. execution times below ~30ms not included."
        }
        if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
            undefine yMin yMax
        }
        call "scripts/setYRange.gnu" \
            "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
            "u (filterPoints(i, e, $0, 100.*($8+$10)/$6))" \
            'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]' \
            'get_skipNPoints(i, e) >= 0'

        # Force logscale or linear scale if asked
        # Modify also variable isYLog created by setYRange
        if (exists("yLogscale")) {
            if (yLogscale == 1) {
                set log y
                isYLog = 1
            } else {
                unset log y
                isYLog = 0
            }
        }

        set format y "%g %%"

        if (exists("xKey") && exists("yKey")) {
            set key at xKey, yKey sp keySpacing h keyHeight
        } else {
            set key top left sp keySpacing h keyHeight
        }

        set term png font fontSettings size sizeWidth,sizeHeight
        set output graphName

        # I do not know a good way to calculate SEM of X/Y.
        plot for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:(filterPoints(i, e, $0, 100*($8+$10)/$6)) \
            w lp ls fls(i, e) t e." ".word(solNames, i+1)
    }
}

if (plotRelativeValues) {
# Objectives:
#
#  1. Determine the maximum number of records, whether or not some rows are
#     invalid (I want the a vector of reference values to manage every index
#     even if it is invalid or not calculated for itself.
#
#  2. Determine the reference as the first pair (e, i) (in the provided order)
#     with at least one valid record for every plot so all of them can use the
#     same pair as a reference.
#
#     Digression:
#        Another option would be to use as a reference the first pair with the
#        biggest number of valid records in every graph, but that does not mean
#        that every other pair can then be compared with it, one pair can have
#        lower valid indices due to precision but another can have higher
#        indices because more extensive computations were done. Better to leave
#        the decision to the user.
#
#  3. Determine if there is something to plot for each graph and plot it.

    set ytics auto

    if (exists("xKey") && exists("yKey")) {
        set key at xKey, yKey sp keySpacing h keyHeight
    } else {
        set key top left sp keySpacing h keyHeight
    }

    if (strstrt(plots, "CPU")) {
        mostRestrictiveCondition = "$8+$10>30 && $6>30"
    } else {
        if (strstrt(plots, "prog")) {
            mostRestrictiveCondition = "$8+$10>30"
        } else {
            mostRestrictiveCondition = "1"
        }
    }
    # ( Adapting code from "skipPoints.gnu". Explanations about the use of
    # "u (cond ? -$0 : $0)" can be found there. )
    maxRecords = -1
    eOfFirstValidRefFile = ""
    iOfFirstValidRefFile = ""
    do for [e in kotlinExecs] for [i in sols] {
        undefine STATS_records
        eval "stats inFilesBeginning.e.inFilesMiddle.i.inFilesEnding " \
            ."u (".mostRestrictiveCondition." ? -$0 : $0) nooutput"
        # Only first condition is needed in my gnuplot version, but the second
        # does not harm.
        if (! exists("STATS_records") || STATS_records == 0) {
            print 'common.gnu: Error: File ' \
                .inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
                .' has no data!'
            exit
        }
        if (STATS_index_max == 0) {
            maxSkippedPoints = 0
        } else {
            if (STATS_index_min == 0) {
                maxSkippedPoints = -STATS_records
            } else {
                maxSkippedPoints = STATS_index_max + 1
            }
        }
        if (STATS_records > maxRecords) {
            maxRecords = STATS_records
        }
        if (eOfFirstValidRefFile eq "" && \
                STATS_records - abs(maxSkippedPoints) > 0) {
            eOfFirstValidRefFile = e
            iOfFirstValidRefFile = i
        }
    }
    # maxSkippedPoints was only used to choose the reference.
    # To choose the points for each graph we will use skipNPoints and refV.
    call "scripts/NDimArray.gnu" "refV" "".maxRecords."i ".words(plots) \
        "0 ".plots
    # It is possible that the reference has invalid records, avoid access to
    # uninitialized elements by initializing all of them to NaN
    do for [r = 0:maxRecords-1] for [p in plots] {
        data_refV[flatInd_refV(r, p)] = NaN
    }
    refFileName = inFilesBeginning.eOfFirstValidRefFile.inFilesMiddle \
        .iOfFirstValidRefFile.inFilesEnding
    # Build a command to initialize refV by calling stats
    initrefV = ""
    if (strstrt(plots, "fun")) {
        initrefV = initrefV.'(set_refV($0, "fun", $2)),'
    }
    if (strstrt(plots, "mem")) {
        initrefV = initrefV.'(set_refV($0, "mem", $4)),'
    }
    if (strstrt(plots, "prog")) {
        initrefV = initrefV.'(set_refV($0, "prog", $8+$10 > 30 \
            ? $8+$10 : NaN)),'
    }
    if (strstrt(plots, "CPU")) {
        # No need to multiply values by 100 since they the reference values and
        # compared ones are going to be divided between them.
        initrefV = initrefV.'(set_refV($0, "CPU", $8+$10>30 && $6>30 \
            ? ($8+$10)/$6 : NaN)),'
    }
    initrefV = initrefV[:strlen(initrefV)-1]
    # Dummy stats, just to read values of refFileName
    eval "stats refFileName u (".initrefV.") nooutput"

    if (strstrt(plots, "fun")) {
        set yl "Relative function execution time"

        set term png font fontSettings size sizeWidth,sizeHeight
        set output outDir."graphRelativeFunctExecTime" \
            .outFilesMiddle.outFilesEnding

        if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
            undefine yMin yMax
        }
        call "scripts/setYRange.gnu" \
            "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" "u 2" \
            'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]'

        # Force logscale or linear scale if asked
        # Modify also variable isYLog created by setYRange
        if (exists("yLogscale")) {
            if (yLogscale == 1) {
                set log y
                isYLog = 1
            } else {
                unset log y
                isYLog = 0
            }
        }

        # If x and/or y are/is NaN, then x/y == NaN. (last '/' means division)
        plot for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:($2/get_refV($0, "fun")) w lp ls fls(i, e) \
            t e." ".word(solNames, i+1)
    }

    if (strstrt(plots, "mem")) {
        set yl "Relative maximum resident set size"

        set term png font fontSettings size sizeWidth,sizeHeight
        set output outDir."graphRelativeMem".outFilesMiddle.outFilesEnding

        if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
            undefine yMin yMax
        }
        call "scripts/setYRange.gnu" \
            "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" "u 2" \
            'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]'

        # Force logscale or linear scale if asked
        # Modify also variable isYLog created by setYRange
        if (exists("yLogscale")) {
            if (yLogscale == 1) {
                set log y
                isYLog = 1
            } else {
                unset log y
                isYLog = 0
            }
        }

        plot for [e in kotlinExecs] for [i in sols] \
            inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
            u 1:($4/get_refV($0, "mem")) w lp ls fls(i, e) \
            t e." ".word(solNames, i+1)
    }

    if (strstrt(plots, "prog")) {

        graphName = outDir."graphRelativeProgramExecTime" \
            .outFilesMiddle.outFilesEnding

        call "scripts/skipPoints.gnu" \
            "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
            "$8+$10>30" "data_skipNPoints[flatInd_skipNPoints(i, e)]" \
            "for [e in kotlinExecs] for [i in sols]"

        # Manage if some or all files must be excluded
        if (! someDataConsidered) {
            print "WARNING: Following graph was not created (program execution times below 30 ms):"
            print graphName
        } else {
            if (someDataSkipped) {
                print "Note: Program execution times below ~30ms not included."
            }
            set yl "Relative program execution time (System+User)"

            set term png font fontSettings size sizeWidth,sizeHeight
            set output graphName

            if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
                undefine yMin yMax
            }
            call "scripts/setYRange.gnu" \
                "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
                "u 2" \
                'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]'

            # Force logscale or linear scale if asked
            # Modify also variable isYLog created by setYRange
            if (exists("yLogscale")) {
                if (yLogscale == 1) {
                    set log y
                    isYLog = 1
                } else {
                    unset log y
                    isYLog = 0
                }
            }

            plot for [e in kotlinExecs] for [i in sols] \
                inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
                u 1:(filterPoints(i, e, $0, ($8 + $10)/get_refV($0, "prog"))) \
                w lp ls fls(i, e) t e." ".word(solNames, i+1)
        }
    }

    if (strstrt(plots, "CPU")) {

        graphName = outDir."graphRelativeCPU".outFilesMiddle.outFilesEnding

        call "scripts/skipPoints.gnu" \
            "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
            "$8+$10>30 && $6>30" \
            "data_skipNPoints[flatInd_skipNPoints(i, e)]" \
            "for [e in kotlinExecs] for [i in sols]"

        # Manage if some or all files must be excluded
        if (! someDataConsidered) {
            print "WARNING: Following graph was not created (prog. execution times below 30 ms):"
            print graphName
        } else {
            if (someDataSkipped) {
                print "Note: CPU percentage values for prog. execution times below ~30ms not included."
            }
            set yl "Relative percentage of CPU used by the entire program"

            set term png font fontSettings size sizeWidth,sizeHeight
            set output graphName

            if (exists("is_yMin_yMax_read") && is_yMin_yMax_read == 0) {
                undefine yMin yMax
            }
            call "scripts/setYRange.gnu" \
                "inFilesBeginning.e.inFilesMiddle.i.inFilesEnding" \
                "u 2" \
                'for [e in "'.kotlinExecs.'"] for [i in "'.sols.'"]'

            # Force logscale or linear scale if asked
            # Modify also variable isYLog created by setYRange
            if (exists("yLogscale")) {
                if (yLogscale == 1) {
                    set log y
                    isYLog = 1
                } else {
                    unset log y
                    isYLog = 0
                }
            }

            plot for [e in kotlinExecs] for [i in sols] \
                inFilesBeginning.e.inFilesMiddle.i.inFilesEnding \
                u 1:(filterPoints(i, e, $0, \
                    (($8+$10)/$6)/get_refV($0, "CPU"))) \
                w lp ls fls(i, e) t e." ".word(solNames, i+1)
        }
    }
}
