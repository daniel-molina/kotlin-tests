function [pdf, midpoints, Xstats] = get_pdf (X, addSmallValue = 1, N = 20)
  sizeDim2 = size(X, 2);
  sizeDim3 = size(X, 3);
  Xstats = statistics(X);
  ## Formulaes for implementing manually a logspace.
  ##
  ##    Delta = 10^((B-A)/(N-1)) = (10^B/10^A)^(1/(N-1))
  ##    edges(k) = Xstats(1) * Delta^(k-1)

  ## Create N+1 edges values (pdfs with N bins) per X column.
  ## However, calculate Delta supposing N edges.
  ## Explanation:
  ## * I want first and last bin to be cero to display better the pdfs.
  ## * histc associates to bin(end) values that are identical to edges(end)
  ##   (Note: it is very improbable for a value to have exactly that value).
  ## * Defining (without floating point errors) edges(end) == 10^B will imply
  ##   that 10^B is included by histc in edges (in particular in edges(end)).
  ## * I will include move rare values from bin(end) to bin(end-1).
  ## * That way bin(end) == 0.
  ## * I will add an additional initial bin with 0 counts but its starting
  ##   edge is not known. Because of that, Delta should be calculated for N-1
  ##   bins (N edges) and then obtain initial edge as edge(2)/Delta.

  # size (Delta) will be [1, sizeDim2, sizeDim3]
  Delta = (Xstats(5,:,:)./Xstats(1,:,:)).^(1/(N-2));
  edges = zeros(N+1, sizeDim2, sizeDim3);
  for k = 1:sizeDim3
    edges(:,:,k) = ...
      [ Xstats(1,:,k) ./ Delta(1,:,k); Xstats(1,:,k);
        Xstats(1,:,k) .* Delta(1,:,k) .^ ((1:N-3).');
        Xstats(5,:,k); Xstats(5,:,k) .* Delta(1,:,k) ];
  endfor

  # Define counts initially reduced.
  counts = zeros (size (edges(1:end-1,:,:)));
  for k = 1:sizeDim3
    for j = 1:sizeDim2
      counts(2:end,j,k) = histc (X(:,j,k), edges(2:end-1,j,k));
    endfor
  endfor
  ## Include values matching exactly last edge in last but one bin.
  counts(end-1,:,:) += counts(end,:,:);
  counts(end,:,:) = 0;
  ## Normalize counts so 'pdf' can be interpreted as a real pdf.
  ## Since bins has different sizes, you must divide also by the bin width.
  counts ./= sum (counts, 1);
  pdf = counts ./ (edges(2:end,:,:) - edges(1:end-1,:,:));
  if (addSmallValue)
    ## Add a small value relative the minimum non-zero probability
    ## found in pdf
    smallValue = min(pdf(pdf != 0)(:))/100;
    pdf(pdf == 0) = smallValue;
  endif
  ## After this point: size (midpoints, 1) = N
  midpoints = (edges(2:end,:,:) + edges(1:end-1,:,:))/2;
endfunction
