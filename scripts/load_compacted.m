function [X, labels1, labels2] = load_compacted (strBegin, vals1, strMid,
                                                  vals2, strEnd,
                                                  reverseDims = false)
  ## -- load_compacted (strBegin, vals1, strEnd)
  ## -- [X, labels] = load_times (strBegin, vals1, strEnd)
  ## -- load_times (strBegin, vals1, strMid, vals2, strEnd)
  ## -- load_times (strBegin, vals1, strMid, vals2, strEnd, reverseDims)
  ## -- [X, labels1, labels2] = load_times (strBegin, vals1, strMid, vals2,
  ##                                        strEnd, ...)
  ##      Return data from different files with one column compacted in an
  ##      array.
  ##
  ##      strBegin must be a string with the initial common part of the
  ##      filenames and strEnd a string with the final common part. If
  ##      provided, strMid must be the string of the middle common part.
  ##
  ##      vals1 and vals2 indicates how to choose the different part of the
  ##      filenames. The format rules for vals1 and vals2 has the same rules,
  ##      but they do no need to share the same format. If it is a cell array
  ##      of strings, the content is understood as the unique strings of the
  ##      filenames. If it is a vector, the behaviour is equivalent to pass a
  ##      cell array which includes a string representation of the array
  ##      elements. If it is a number, the bahaviour is equivalent to pass a
  ##      vector which elements are power of 2 starting from 1 and finishing
  ##      at smallest power which is bigger than provided value. If it is an
  ##      empty matrix, the behaviour is equivalent to pass a single number.
  ##      This particular number will be chosen by checking the current
  ##      filenames to the biggest consecutive power of two (starting from 1)
  ##      that is matched.
  ##
  ##      Data coming from the same file is saved in the same column of the
  ##      returned array. By default, different files matching vals1 are
  ##      stored in the second dimension of the array and those from vals2 in
  ##      the third dimension. If reverseDims is set to true the behaviour is
  ##      the opposite.
  ##
  ##      labels1 will be a cell array with the unique strings used according
  ##      to vals1. labels2 is the counterpart of vals2.

  if (nargin != 3 && nargin != 5 && nargin != 6)
    error ("Incorrect number of input arguments.")
    return
  endif

  if (nargout > 3 && nargin == 5)
    error ("Only 3 output arguments allowed.")
  endif

  if (nargout > 2 && nargin == 3)
    error ("Only 2 output arguments allowed.")
  endif

  if (nargin == 3)
    [X, labels1] = load_compacted (strBegin, vals1, "", {""}, strMid, true);
    return
  endif

  ## Get labels
  labels1 = valsToCellArray (vals1);
  labels2 = valsToCellArray (vals2);
  [labels1, labels2] = completeLabels (strBegin, labels1, strMid, labels2,
                        strEnd);

  N1 = numel (load (strcat (strBegin, labels1{1}, strMid, labels2{1},
        strEnd)));
  [N2, N3] = correctDims (length (labels1), length (labels2), reverseDims);
  X = zeros (N1, N2, N3);

  for i3 = 1:N3
    for i2 = 1:N2
      [l1i, l2i] = correctDims (i2, i3, reverseDims);
      X(:,i2,i3) = load (strcat (strBegin, labels1{l1i}, strMid, labels2{l2i},
                    strEnd));
    endfor
  endfor
endfunction

function cellArray = cellArrayFromFilesMatchingPowersOf2 (fileName_fun)
  w = 1;
  wstr = "1";
  cellArray = {};
  while (exist (fileName_fun (wstr), "file"))
    cellArray{end+1,1} = wstr;
    wstr = num2str (w *= 2);
  endwhile
  if (isempty (cellArray))
    error ("No matching file exists.")
  endif
endfunction

function cellArray = valsToCellArray (vals)
  ## Returns the equivalent cell array.
  ## If vals is an empty matrix, it returns it instead.
  if (iscellstr (vals))
    cellArray = vals;
  elseif (isscalar (vals))
    if (vals == 1)
      cellArray = {"1"};
    else
      lastExp = ceil (log2 (vals));
      cellArray = valsToCellArray (2 .^ (0:lastExp).');
    endif
  elseif (isvector (vals))
    cellArray = arrayfun (@num2str, vals, "UniformOutput", false);
  elseif (isempty (vals))
    ## flag value, not really a cell array
    cellArray = vals;
  else
    error ("Invalid input argument.")
  endif
endfunction

# Get data from files
function [newl1, newl2] = correctDims (l1, l2, isreversed)
  if (! isreversed)
    newl1 = l1;
    newl2 = l2;
  else
    newl1 = l2;
    newl2 = l1;
  endif
endfunction

function [newl1, newl2] = completeLabels(strS, l1, strM, l2, strE)
  if (iscellstr (l1) && iscellstr (l2))
    newl1 = l1;
    newl2 = l2;
  elseif (iscellstr (l1) && ! iscellstr (l2))
    newl1 = l1;
    newl2 = cellArrayFromFilesMatchingPowersOf2 (
      @(x) strcat (strS, l1{1}, strM, x, strE));
  elseif (! iscellstr (l1) && iscellstr (l2))
    newl1 = cellArrayFromFilesMatchingPowersOf2 (
      @(x) strcat (strS, x, strM, l2{1}, strE));
    newl2 = l2;
  else
    newl1 = cellArrayFromFilesMatchingPowersOf2 (
      @(x) strcat (strS, x, strM, "1", strE));
    newl2 = cellArrayFromFilesMatchingPowersOf2 (
      @(x) strcat (strS, "1", strM, x, strE));
  endif
endfunction