#!/bin/bash

# By the moment the DB only manages an excerpt, no the inividual results.

# Security note:
# Trust in the correctness of the values is placed in the caller, which
# actually checks their types. I do not consider that this interface for the DB
# needs more security on the input data as long as it is used that way.

# It depends on manage_opts.sh (errcho, prettyList).
# source it before doing so with this.

# Check sqlite
if ! sqlite3 -version >& /dev/null; then
    errcho "sqlite3 command not found."
fi

# Internal use
# manageDBWrongNumberOfArgs "$@"
# It manages correctly empty arguments
manageDBWrongNumberOfArgs() {
    local args msg
    args=$(prettyList "$@")
    msg="${FUNCNAME[1]} called with an invalid number of arguments by "
    msg+="${FUNCNAME[2]}. Arguments: ${args:-No arguments}."
    echo >&2 "manage_db: $msg"
    exit 1
}

# Internal use
# manageDBWrongVarNameErr <wrong_var_name>
manageDBWrongVarNameErr() {
    [[ $# -eq 1 ]] || manageDBWrongNumberOfArgs "$@"
    >&2 echo "manage_db: Wrong argument passed to ${FUNCNAME[1]}. \"$1\" is not a variable name."
    exit 1
}

testsDB=
# setDBPath <db_path>
#Set the path to the database
setDBPath() {
    testsDB="$1"
}

# initDBIfNeeded
#
# Create tables if they were not created before.
initDBIfNeeded() {
    [[ $# -eq 0 ]] || manageDBWrongNumberOfArgs "$@"
    sqlite3 "$testsDB" \
        "CREATE TABLE IF NOT EXISTS average (
            N INTEGER NOT NULL CHECK (N > 0),
            exec TEXT NOT NULL
                CHECK (exec = 'JVM' OR exec = 'opt_native' OR exec = 'native'),
            orders INTEGER NOT NULL CHECK (orders > 0),
            prods INTEGER NOT NULL CHECK (prods > 0),
            sol INTEGER NOT NULL CHECK (sol >= 0),
            max_calls INTEGER NOT NULL CHECK (max_calls > 0),
            UNIQUE(N, exec, prods, orders, sol)
        );"

    sqlite3 "$testsDB" \
        "CREATE TABLE IF NOT EXISTS total (
            N INTEGER NOT NULL CHECK (N > 0),
            exec TEXT NOT NULL
                CHECK (exec = 'JVM' OR exec = 'opt_native' OR exec = 'native'),
            calls INTEGER NOT NULL CHECK (calls > 0),
            sol INTEGER NOT NULL CHECK (sol >= 0),
            max_orders INTEGER NOT NULL CHECK (max_orders > 0),
            UNIQUE(N, exec, calls, sol)
        );"

    sqlite3 "$testsDB" \
        "CREATE TABLE IF NOT EXISTS ratio (
            N INTEGER NOT NULL CHECK (N > 0),
            exec TEXT NOT NULL
                CHECK (exec = 'JVM' OR exec = 'opt_native' OR exec = 'native'),
            calls INTEGER NOT NULL CHECK (calls > 0),
            sol INTEGER NOT NULL CHECK (sol >= 0),
            total_prods INTEGER NOT NULL CHECK (total_prods > 0),
            max_orders INTEGER NOT NULL CHECK (max_orders > 0),
            UNIQUE(N, exec, calls, sol, total_prods)
        );"
}

# Previously, code below used UPSERT in the canonical form:
# INSERT INTO tbl VALUES(...) ON CONFLICT (...) DO UPDATE SET ... WHERE ... )
# However, that requires sqlite 3.24.0 or above, which is not always in
# repositories. Ubuntu 18.04 includes sqlite 3.22.0, for example.
# --> Now it is used INSERT OR IGNORE + UPDATE.
# It has a drawback, wrong passed values that not compatible with table's CHECK
# will be silently ignored and, of course, no row will be inserted or updated at
# all. Once more, security relays on value checks done by the caller before
# calling.
# 
# Note: I do not find the need to use a transaction for this operation.

# averageTestUpdate <N> <e> <o> <p> <s> <max_calls>
averageTestUpdate() {
    [[ $# -eq 6 ]] || manageDBWrongNumberOfArgs "$@"
    sqlite3 "$testsDB" \
        "INSERT OR IGNORE INTO average (N, exec, orders, prods, sol, max_calls)
        VALUES($1, '$2', $3, $4, $5, $6);

        UPDATE average
        SET max_calls=$6
        WHERE N=$1
              AND exec='$2'
              AND orders=$3
              AND prods=$4
              AND sol=$5
              AND $6 > max_calls;"
}

# totalTestUpdate <N> <e> <c> <s> <max_orders>
totalTestUpdate() {
    [[ $# -eq 5 ]] || manageDBWrongNumberOfArgs "$@"
    sqlite3 "$testsDB" \
        "INSERT OR IGNORE INTO total (N, exec, calls, sol, max_orders)
        VALUES($1, '$2', $3, $4, $5);

        UPDATE total
        SET max_orders=$5
        WHERE N=$1
              AND exec='$2'
              AND calls=$3
              AND sol=$4
              AND $5 > max_orders;"
}

# ratioTestUpdate <N> <e> <c> <s> <total_prods> <max_orders>
ratioTestUpdate() {
    [[ $# -eq 6 ]] || manageDBWrongNumberOfArgs "$@"
    sqlite3 "$testsDB" \
        "INSERT OR IGNORE INTO ratio(N, exec, calls, sol, total_prods, max_orders)
        VALUES($1, '$2', $3, $4, $5, $6);

        UPDATE ratio
        SET max_orders=$6
        WHERE N=$1
              AND exec='$2'
              AND calls=$3
              AND sol=$4
              AND total_prods=$5
              AND $6 > max_orders;"
}

# array2csv [arg1] [arg2] ... [argN]
# Output: 'arg1', 'arg2', ... , 'argN'
array2csv() {
    [[ $# -ne 0 ]] || return
    [[ $# -ne 1 ]] || { echo "'$1'"; return; }
    local args
    while : ; do
        if [[ $# -ne 0 ]]; then
            args+="'$1', "
        else
            echo "${args%??}"
            return
        fi
        shift
    done
}

# averageDisplay <N> <var_name_execs> <orders> <prods> <var_name_sols> <max_calls>
# totalDisplay <N> <var_name_execs> <calls> <var_name_sols> <max_orders>
# ratioDisplay <N> <var_name_execs> <calls> <var_name_sols> <total_prods> <max_orders>
#
# Show the excerpt of all test compatible with the parameters passed to this
# function. The excerpt groups execs and sols whenever it is possible.
# Output is ready to fit in a 80 columns wide.
#
# For <var_name_execs> and <var_name_sols>, the name of the array variables
# are expected. For the rest, use the value to show or '%' to include all of
# them.
averageDisplay() {
    [[ $# -eq 6 ]] || manageDBWrongNumberOfArgs "$@"
    local -n execsADRef=$2 || manageDBWrongVarNameErr "$2"
    local -n solsADRef=$5 || manageDBWrongVarNameErr "$5"

    local output=$(sqlite3 "$testsDB" \
        ".mode column" ".header on" ".width 11 7 14 14 9 15" \
        "SELECT N,
                CASE COUNT(exec)
                    WHEN 3 THEN 'All'
                    ELSE group_concat(substr(exec, 1, 3))
                END AS exec,
                orders,
                prods AS products,
                gsol AS sol,
                max_calls AS 'max calls'
        FROM (
                SELECT N,
                       exec,
                       orders,
                       prods,
                       CASE COUNT(sol)
                            WHEN 6 THEN 'All'
                            ELSE group_concat(sol)
                       END AS gsol,
                       max_calls
                FROM average
                WHERE N LIKE '$1'
                      AND exec IN ($(array2csv ${execsADRef[@]}))
                      AND orders LIKE '$3'
                      AND prods LIKE '$4'
                      AND sol IN ($(array2csv ${solsADRef[@]}))
                      AND max_calls LIKE '$6'
                GROUP BY N, exec, orders, prods, max_calls
             )
        GROUP BY N, orders, prods, sol, max_calls
        ORDER BY max_calls DESC, N DESC, orders DESC, prods DESC, exec;")

    echo "--> Test Changing Calls (average):"
    if [[ -n $output ]]; then
        local msg="Note: Only the highest number of calls is shown "
        msg+="for each set of parameters."
        echo "$msg"
        echo "$output"
    else
        echo "No test is registered with the specified parameters."
    fi
}

# See averageDisplay
totalDisplay() {
    [[ $# -eq 5 ]] || manageDBWrongNumberOfArgs "$@"
    local -n execsTDRef=$2 || manageDBWrongVarNameErr "$2"
    local -n solsTDRef=$4 || manageDBWrongVarNameErr "$4"

    local output=$(sqlite3 "$testsDB" \
        ".mode column" ".header on" ".width 15 7 15 9 15" \
        "SELECT N,
                CASE COUNT(exec)
                    WHEN 3 THEN 'All'
                    ELSE group_concat(substr(exec, 1, 3))
                END AS exec,
                calls,
                gsol AS sol,
                max_orders AS 'max orders'
        FROM (
                SELECT N,
                       exec,
                       calls,
                       CASE COUNT(sol)
                            WHEN 6 THEN 'All'
                            ELSE group_concat(sol)
                       END AS gsol,
                       max_orders
                FROM total
                WHERE N LIKE '$1'
                      AND exec IN ($(array2csv ${execsTDRef[@]}))
                      AND calls LIKE '$3'
                      AND sol IN ($(array2csv ${solsTDRef[@]}))
                      AND max_orders LIKE '$5'
                GROUP BY N, exec, calls, max_orders
             )
        GROUP BY N, calls, sol, max_orders
        ORDER BY max_orders DESC, N DESC, calls DESC, exec;")

    echo "--> Test Changing Total Products (total):"
    if [[ -n $output ]]; then
        local msg="Note: Only the highest number of orders is shown "
        msg+="for each set of parameters."
        echo "$msg"
        echo "$output"
    else
        echo "No test is registered with the specified parameters."
    fi
}

# See averageDisplay
ratioDisplay() {
    [[ $# -eq 6 ]] || manageDBWrongNumberOfArgs "$@"
    local -n execsRDRef=$2 || manageDBWrongVarNameErr "$2"
    local -n solsRDRef=$4 || manageDBWrongVarNameErr "$4"

    local output=$(sqlite3 "$testsDB" \
        ".mode column" ".header on" ".width 11 7 13 9 15 15" \
        "SELECT N,
                CASE COUNT(exec)
                    WHEN 6 THEN 'All'
                    ELSE group_concat(substr(exec, 1, 3))
                END AS exec,
                calls,
                gsol AS sol,
                total_prods AS 'total products',
                max_orders AS 'max orders'
        FROM (
                SELECT N,
                       exec,
                       calls,
                       CASE COUNT(sol)
                            WHEN 5 THEN 'All'
                            ELSE group_concat(sol)
                       END AS gsol,
                       total_prods,
                       max_orders
                FROM ratio
                WHERE N LIKE '$1'
                      AND exec IN ($(array2csv ${execsRDRef[@]}))
                      AND calls LIKE '$3'
                      AND sol IN ($(array2csv ${solsRDRef[@]}))
                      AND total_prods LIKE '$5'
                      AND max_orders LIKE '$6' 
                GROUP BY N, exec, calls, total_prods, max_orders
             )
        GROUP BY N, calls, sol, total_prods, max_orders
        ORDER BY max_orders DESC, total_prods DESC, N DESC, calls DESC, exec;")

    echo "--> Test Changing Products-Order Ratio (ratio):"
    if [[ -n $output ]]; then
        echo "$output"
    else
        echo "No test is registered with the specified parameters."
    fi
}

