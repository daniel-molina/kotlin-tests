# manage_opts.sh 0.1.0
#
# This is free and unencumbered software released into the public domain.
# For more information, please refer to <https://unlicense.org>
#-------------------------------------------------------------------------------
#
# Brief: Bash library to manage options passed by the user. Not a substitute
#        of a getopt-like program, but a complement.
#
# Notation:
#   * An user passes "options" to a program, with an "argument" or without it.
#     getopt's non-option parameters can be used as options without arguments.
#
# Motivation: It was created with the intention of automatically check
#             if an option is being passed more than once and if it satisfies
#             some criteria, like being a natural or real number.
#             It was difficult to have meaningful names for functions doing
#             these tasks without implementing this as a library.
#
# Notes: When using this library, two kind of errors are managed:
#
#   * Internal errors due to a wrong use of the library.
#     They do not occur if the programmer made no mistakes.
#
#       They are preceded by "manage_opts: ".
#
#   * Planned errors which inform the user that introduced options are wrong.
#
#       They are preceded by "Error: ".
#
# Usage:
#
#   * Load this bash script:
#
#       source "manage_opts.sh"
#
#   * Add options with arguments or without them:
#
#       addOpt <opt> [arg [default_arg]]
#       addDistinctOpt <opt> [arg [default_arg]]
#
#     Note 1: Options cannot be empty strings. Arguments can.
#     Note 2: Options with arguments and without them are treated separately.
#             There are no collisions when they share options with the same
#             names.
#
#   * Set a default value for an option in case it is not added.
#
#       setDefaultArg <opt> <arg>
#
#           Note: You can use it after addOpt <opt> <arg> [def] with no effect.
#
#   * Check if an option was previously added or a default value was set:
#
#       isOptWithArg <opt>
#       isOptWithoutArg <opt>
#
#   * Check if option with argument was explicitly added with addOpt.
#
#       isOptWithArgAdded <opt>
#   * Access the argument of a option saved in a variable:
#
#       getArg <opt> <var_name>
#
#               Note: If no option was added nor a default was set, you will
#                     get an error.
#               Note: Do not include a $ in <var_name>.
#
#   * Assure that the argument of an option satisfy certain conditions:
#
#     In particular, it is possible to assert that it can be used as
#     directory to write files on it. The directory will be created if it does
#     not exist.
#
#       errorIfArgIsNot0Or1          <opt>
#       errorIfArgIsNotNatural       <opt>
#       errorIfArgIsNotReal          <opt>
#       errorIfArgIsNotAnExistingDir <opt>
#       errorIfCannotWriteFilesInArg <opt>
#
#  * Others:
#
#       setArgNextPowerOf2 <opt>
#
# Created/Modified variables:
#
#  * opts_aa: an associative array which stores options with arguments.
#  * optsdef_aa
#  * optswa_aa: an associative array which stores options without arguments.
#
#  (They are managed separately to not use special values for marking options
#   without parameters)
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Usage: errcho <error_message> [another_message] [and_another] ...
#-------------------------------------------------------------------------------
# Brief: Send messages to stderr and exit with a status of 1.
#
# Motivation: It is possible to split a message in sevaral lines with echo -e.
#             However, I don't want to use it along arbitrary user input.
#             Solution provided is to use different arguments for every line.
#
# Description:
#
#   * With only one argument, send it to stderr preceded by "Error: " and exit.
#   * If more arguments are present, each of them will be printed in different
#     lines. No modification is done to any of the arguments.
#-------------------------------------------------------------------------------
errcho() {
    if [[ $# -eq 1 ]]; then
        >&2 echo "Error: $1"
        exit 1
    else
        for i in "$@"; do >&2 echo "$i"; done
        exit 1
    fi
}

# prettyList [arg1] [arg2] ... [argN]
# Output: "arg1", "arg2", ... and "argN"
# Not using first argument as a nameref to keep code simpler and safer:
# This function is used by functions managing errors so I cannot use them
# without the risk of circular function calls.
prettyList() {
    [[ $# -ne 0 ]] || return
    [[ $# -ne 1 ]] || { echo "\"$1\""; return; }
    local args
    while : ; do
        if [[ $# -ne 1 ]]; then
            args+="\"$1\", "
        else
            args="${args%??}"
            echo "$args and \"$1\""
            return
        fi
        shift
    done
}

# Internal use
# wrongNumberOfArgs "$@"
# It manages correctly empty arguments
wrongNumberOfArgs() {
    local args msg
    args=$(prettyList "$@")
    msg="${FUNCNAME[1]} called with an invalid number of arguments by "
    msg+="${FUNCNAME[2]}. Arguments: ${args:-No arguments}."
    echo >&2 "manage_opts: $msg"
    exit 1
}

# Internal use
syntaxErr() {
    [[ $# -eq 1 ]] || wrongNumberOfArgs "$@"
    >&2 echo "manage_opts: $1"
    exit 1
}

# Internal use
# manageOptsWrongVarNameErr <wrong_var_name>
manageOptsWrongVarNameErr() {
    [[ $# -eq 1 ]] || wrongNumberOfArgs "$@"
    >&2 echo "manage_opts: Wrong argument passed to ${FUNCNAME[1]}. \"$1\" is not a variable name."
    exit 1
}

# Internal use
# manageCommonErrors <"$#"> <patternNArgs> <opt> <"check"|"nocheck"> <"$@"> 
#
#  0*. Make sure that a correct number of arguments was passed to this function.
#  1.  Make sure that option passed to the caller is not empty.
#  2.  If "check", make sure that <opt> was added as an option with argument
#      or a default argument was set for it.
#  3*. Make sure that only "check" or "nocheck" was passed to this function.
#  4.  Make sure that the caller was called with a valid number of arguments.
manageCommonErrors() {
    # Local safety check
    if [[ $# -lt 4 ]]; then
        wrongNumberOfArgs "$@"
    fi
    # Check that <opt> is not "" before (maybe) checking if it was added
    if [[ -z $3 ]]; then
        syntaxErr "Option passed to ${FUNCNAME[1]} was an empty string."
    elif [[ $4 = "check" ]]; then
        isOptWithArg "$3" || syntaxErr "Option \"$3\" passed to ${FUNCNAME[1]} was not previously added."
    elif [[ $4 != "nocheck" ]]; then
        syntaxErr "${FUNCNAME[0]} called with an incorrect 4th argument by ${FUNCNAME[1]}."
    fi
    # Check that number of arguments was correct
    if [[ $1 != [$2] ]]; then
        shift 4;
        local args msg
        args=$(prettyList "$@")
        msg="${FUNCNAME[1]} called with an invalid number of arguments by "
        msg+="${FUNCNAME[2]}. Arguments: ${args:-No arguments}."
        syntaxErr "$msg"
    fi
}

# Associative arrays
declare -A opts_aa optsdef_aa optswa_aa

# isOptWithArgAdded <opt>
#
# Return 0 only if <opt> was added through addOpt.
# isOptWithArg returns 0 even if the associated argument is an empty string.
isOptWithArgAdded() {
    manageCommonErrors $# 1 "$1" "nocheck" "$@"
    [[ -n ${opts_aa[$1]+x} ]]
}

# isOptWithoutArg <opt>
#
# Return 0 only if <opt> was previosuly added.
isOptWithoutArg() {
    manageCommonErrors $# 1 "$1" "nocheck" "$@"
    [[ -n ${optswa_aa[$1]+x} ]]
}

# isOptWithArg <opt>
#
# Return 0 if <opt> was added or a default was set for it through setDefaultArg.
# isOptWithArg returns 0 even if the associated argument is an empty string.
isOptWithArg() {
    manageCommonErrors $# 1 "$1" "nocheck" "$@"
    [[ -n ${opts_aa[$1]+x} ]] || [[ -n ${optsdef_aa[$1]+x} ]]
}

# getArg <var_name> <opt>
#
# Assign stored value of <opt> in variable named <var_name>.
#
# Warning 1: Do not include a $ preceding <var_name>.
# Error if <opt> was not added and no default was set for it.
# -> You can use isOptWithArg before if not sure.
getArg() {
    manageCommonErrors $# 2 "$2" "check" "$@"
    local -n gARef=$1 || manageOptsWrongVarNameErr "$1"
    if isOptWithArgAdded "$2"; then
        gARef="${opts_aa[$2]}"
    else
        gARef="${optsdef_aa[$2]}"
    fi
}

# setDefaultArg <opt> <arg>
#
# Set a default value for an option.
# No restrictions on the number of times it can be called for the same <opt>.
setDefaultArg() {
    manageCommonErrors $# 2 "$1" "nocheck" "$@"
    optsdef_aa[$1]="$2"
}

# addOpt <opt> [arg [default_arg]]
#
# Store <opt>, with or whithout argument depending whether [arg] is present.
# If <opt> was already present and [arg] is provided, argument is overwritten
# with [arg]. If [arg] is not present, nothing changes.
# If [default_arg] is provided and [arg] is an empty string, that is equivalent
# to use: addOpt opt default_arg
addOpt() {
    manageCommonErrors $# 123 "$1" "nocheck" "$@"
    if [[ $# -eq 1 ]]; then
        optswa_aa[$1]=""
    elif [[ $# -eq 3 && -z $2 ]]; then
        opts_aa[$1]="$3"
    else
        opts_aa[$1]="$2"
    fi
}

# addDistinctOpt <opt> [arg [default_arg]]
#
# Store new <opt>, with or without argument depending whether [arg] is present.
# If <opt> was already present, report an error and exit.
# If [default_arg] is provided and [arg] is an empty string, that is equivalent
# to use: addDistinctOpt opt default_arg
addDistinctOpt() {
    manageCommonErrors $# 123 "$1" "nocheck" "$@"
    if [[ $# -eq 1 ]]; then
        if isOptWithoutArg "$1"; then
            errcho "Option \"$1\" passed more than once."
        else
            optswa_aa[$1]=""
        fi
    elif isOptWithArgAdded "$1"; then
        errcho "Option \"$1\" passed more than once."
    else
        if [[ $# -eq 3 && -z $2 ]]; then
            opts_aa[$1]="$3"
        else
            opts_aa[$1]="$2"
        fi
    fi
}

# is0Or1 <value>
#
# Return 0 only if argument is identical to 0 or 1.
# Note: It is intended to check logical conditions, so it does not check for
# numerical equality. For example, +1 does not pass the test.
is0Or1() {
    if [[ $# -ne 1 ]]; then wrongNumberOfArgs "$@"; fi
    [[ $1 = [01] ]]
}

# errorIfArgIsNot0Or1 <opt>
#
# Report an error and exit if stored argument of <opt> is not 0 or 1.
errorIfArgIsNot0Or1() {
    manageCommonErrors $# 1 "$1" "check" "$@"
    local arg
    getArg arg "$1"
    if ! is0Or1 "$arg"; then
        errcho "Argument of option \"$1\" must be 0 or 1. Used value: \"$arg\"."
    fi
}

# isNatural <value>
#
# Return 0 only if the argument is a valid natural number.
# 0 is not considered a natural number.
isNatural() {
    if [[ $# -ne 1 ]]; then wrongNumberOfArgs "$@"; fi
    [[ $1 =~ ^\+?[1-9][0-9]*$ ]]
}

# errorIfArgIsNotNatural <opt>
#
# Report an error and exit if value of option <opt> is not a natural
# number.
errorIfArgIsNotNatural() {
    manageCommonErrors $# 1 "$1" "check" "$@"
    local arg
    getArg arg "$1"
    if ! isNatural "$arg"; then
        errcho "Argument of option \"$1\" must be a natural number. Used value: \"$arg\"."
    fi
}

# nextPowerOf2 <var_name> [value]
#
# <var_name> is the name of a variable in which will be saved the smaller power
# of two >= [value]. If [value] is not provided, content of <var_name> will be
# used instead.
#
# Provided value must be a natural number.
nextPowerOf2() {
    [[ $# = [12] ]] || wrongNumberOfArgs "$@"
    local -n nPOf2Ref=$1 || manageOptsWrongVarNameErr "$1"
    if [[ $# -eq 1 ]]; then
        local maxValue="$nPOf2Ref"
    else
        local maxValue="$2"
    fi
    if ! isNatural "$maxValue"; then
        local msg="Wrong value passed to ${FUNCNAME[0]}. "
        msg+="\"$value\" is not a natural number."
        [[ $# -eq 1 ]] && msg+=" Is variable name \"$1\" correctly written?"
        syntaxErr "$msg"
        exit 1
    fi
    # At this point, $maxValue is assured to be strictly bigger than 0

    # If <var_name> is an array element, ((nPOf2Ref)) is always 0.
    # A different variable, n, will be used in the loop for that reason.
    local n
    for (( n=1; n<maxValue; n*=2 )); do : ; done
    nPOf2Ref=$n
}

# setArgNextPowerOf2 <opt>
setArgNextPowerOf2() {
    manageCommonErrors $# 1 "$1" "check" "$@"
    # Maybe using an array element as nameref is not correct.
    # However, it works in the way I am using it.
    # If someone points to me that that is evil, I will use another approach.
    if isOptWithArgAdded "$1"; then
        nextPowerOf2 opts_aa[$1]
    else
        nextPowerOf2 optsdef_aa[$1]
    fi
}

# isReal <value>
#
# Return 0 only if argument is a valid real number.
# NaN and Inf (in many forms) pass this test
isReal() {
    if [[ $# -ne 1 ]]; then wrongNumberOfArgs "$@"; fi
    # First condition is there because empty string satisfies the second one
    # and that is not what is expected
    [[ -n $1 ]] && LC_ALL=C.UTF-8 printf "%f" "$1" >& /dev/null
}

# errorIfArgIsNotReal <opt>
#
# Report an error and exit if stored argument of <opt> is not a real number.
errorIfArgIsNotReal() {
    manageCommonErrors $# 1 "$1" "check" "$@"
    local arg
    getArg "$1" arg
    if ! isReal "$arg"; then
        errcho "Argument of option \"$1\" must be a valid number. Used value: \"$arg\"."
    fi
}

# createDir <dir_path>
#
# Create directory indicated by argument <dir_path> if it does not exist.
# Return 0 only if directory was created correctly o it existed.
#
# Since this function does an action (create a directory), the user could just
# use it for the action and not check the exit code. Because of that, I am not
# suppressing errors of mkdir.
createDir() {
    if [[ $# -ne 1 ]]; then wrongNumberOfArgs "$@"; fi
    mkdir -p "$1"
}

# includeFinalSlashIfAbsent <var_name>
#
# If the value of <var_name> does not finish at slash '/', add it.
# Warning: <var_name> does not include a $.
includeFinalSlashIfAbsent() {
    if [[ $# -ne 1 ]]; then wrongNumberOfArgs "$@"; fi
    local -n iFSIARef=$1 || manageOptsWrongVarNameErr "$1"
    if [[ $iFSIARef != */ ]]; then iFSIARef+='/'; fi
}

# filesCanBeCreated <dir_path>
#
# Return 0 only if files can be created in path specified by <dir_path>
#
# Warning: <dir_path> must finish in '/'.
# -> You can use includeFinalSlashIfAbsent if not sure.
filesCanBeCreated() {
    if [[ $# -ne 1 ]]; then wrongNumberOfArgs "$@"; fi
    [[ -w $1 ]]
    #local dummyFile="${1}dummy_file.sal"
    #touch "$dummyFile" 2> /dev/null && rm "$dummyFile"
}

# errorIfArgIsNotAnExistingDir <opt>
errorIfArgIsNotAnExistingDir() {
    manageCommonErrors $# 1 "$1" "check" "$@"
    # Maybe using an array element as nameref is not correct.
    # However, it works in the way I am using it.
    # If someone points to me that that is evil, I will use another approach.
    if isOptWithArgAdded "$1"; then
        local -n eIAINARDRef="opts_aa[$1]"
    else
        local -n eIAINARDRef="optsdef_aa[$1]"
    fi

    includeFinalSlashIfAbsent eIAINARDRef
    if ! [[ -d $eIAINARDRef ]]; then
        errcho "The following directory does not exist: \"$eIAINARDRef\"."
    fi
}

# errorIfCannotWriteFilesInArg <opt>
#
# Brief: This function assures that stored argument of <opt> can be used as a
#        directory and files can be created in it.
#
# As a side effect, it creates the directory and a slash is added to the end
# of the arg if absent.
#
# Description:
#   1. If absent, append a missing '/' at the end of the argument of <opt>.
#   2. If directory exists, go to step 3.
#      If it does not exist, create it. If not possible, report an error.
#   3. Check that files can be created in the directory. Else, report an error.
#-------------------------------------------------------------------------------
errorIfCannotWriteFilesInArg() {
    manageCommonErrors $# 1 "$1" "check" "$@"
    # Maybe using an array element as nameref is not correct.
    # However, it works in the way I am using it.
    # If someone points to me that that is evil, I will use another approach.
    if isOptWithArgAdded "$1"; then
        local -n eICWFIARef="opts_aa[$1]"
    else
        local -n eICWFIARef="optsdef_aa[$1]"
    fi

    includeFinalSlashIfAbsent eICWFIARef
    if ! createDir "$eICWFIARef"; then
        errcho "You are not allowed to create the following directory: \"$eICWFIARef\"."
    fi
    if ! filesCanBeCreated "$eICWFIARef"; then
        errcho "You are not allowed to create files in the following directory: \"$eICWFIARef\"."
    fi
}
