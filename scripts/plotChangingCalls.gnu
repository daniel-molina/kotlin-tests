# I am not combining parameters (-c) and "call" instructions because
# of a bug in gnuplot 5.2.6. It is reported to be solved in 5.2.7.
# https://sourceforge.net/p/gnuplot/bugs/2115/

# Begin Set parameters --------------------------
N = 10
orders = 100
prods = 100

# plots is a string of words specifying the magnitudes to be plotted.
# Valid words for plots are: fun mem prog CPU
plots = "fun"

# kotlinExecs is a string of words specifying the execs to be plotted.
# Valid words for kotlinExecs are: JVM Native NativeOpt
kotlinExecs = "JVM"

# sols is a string of words specifying the solutions to be plotted.
# Valid words for sols are: 0 1 2 3 4 5
sols = "0 1 2 3 4 5"

#yMin =
#yMax =
#xKey =
#yKey =
plotErrorLines = 0
plotRelativeValues = 0

# If yLogscale is 1 or 0, logscale or linear scale, respectively, are forced
#yLogscale =
fontSize = 40

inDir = "./output/"
outDir = "./"

# End Set parameters -----------------------------

test = "ChangingCalls"

# inFile example: resultsNativeOptChangingCalls_N10_sol3_orders100_prods100.sal
inFilesMiddle = test."_N".N."_sol"
inFilesEnding = "_orders".orders."_prods".prods.".sal"

briefMiddle = "N=".N.", orders=".orders.", prods=".prods

# outFile example: graphProgramExecTimeJVMChangingCalls_N10_orders100_prods100.png
outFilesEnding = test."_N".N."_orders".orders."_prods".prods.".png"

set xl "Number of function calls"

call "scripts/common.gnu"
