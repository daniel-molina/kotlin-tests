# I am not combining parameters (-c) and "call" instructions because
# of a bug in gnuplot 5.2.6. It is reported to be solved in 5.2.7.
# https://sourceforge.net/p/gnuplot/bugs/2115/

# Begin Set parameters --------------------------
N = 10
calls = 1

# plots is a string of words specifying the magnitudes to be plotted.
# Valid words for plots are: fun mem prog CPU
plots = "fun"

# kotlinExecs is a string of words specifying the execs to be plotted.
# Valid words for kotlinExecs are: JVM Native NativeOpt
kotlinExecs = "JVM"

# sols is a string of words specifying the solutions to be plotted.
# Valid words for sols are: 0 1 2 3 4 5
sols = "0 1 2 3 4 5"

#yMin =
#yMax =
#xKey =
#yKey =
plotErrorLines = 0
plotRelativeValues = 0

# If yLogscale is 1 or 0, logscale or linear scale, respectively, are forced
#yLogscale =
fontSize = 40

inDir = "./output/"
outDir = "./"

# End Set parameters -----------------------------

test = "ChangingTotalProducts"

# inFile example: resultsJVMChangingTotalProducts_N10_sol0_calls1.sal
inFilesMiddle = test."_N".N."_sol"
inFilesEnding = "_calls".calls.".sal"

briefMiddle = "N=".N.", calls=".calls

# outFile example: graphProgramExecTimeJVMChangingTotalProducts_N10_calls1.png
outFilesEnding = test."_N".N."_calls".calls.".png"

set xl "Number of products per order * Number of orders"

call "scripts/common.gnu"
