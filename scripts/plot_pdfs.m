function plot_pdfs (tmid, pdf, labels, stats)
  nPlots = size (tmid, 3);
  xRange = [min(stats(1,:,:)(:)), max(stats(5,:,:)(:))];
  if (xRange(2)/xRange(1) >= 100)
    f_plot = @loglog;
  else
    f_plot = @semilogx;
  endif

  sp_margin_h = 0.05;
  sp_margin_v = 0.1;
  sp_w = (1-2*sp_margin_h)/nPlots;
  sp_w_reduced = sp_w* (1 - 0.1);
  sp_h = 1-2*sp_margin_v;
  clf;
  for k = 1:nPlots
    hax = subplot (1, nPlots, k, "position",
        [sp_margin_h+(k-1)*sp_w, sp_margin_v, sp_w_reduced, sp_h]);
    f_plot (pdf(:,:,k), tmid(:,:,k), 'linewidth', 1);
    for j = 1:size (tmid, 2)
      line ("xdata", [min(pdf(:,j,k)), max(pdf(:,j,k))],
          "ydata", [stats(6,j,k), stats(6,j,k)],
          "linestyle", "--")
      line ("xdata", [min(pdf(:,j,k)), max(pdf(:,j,k))],
          "ydata", [stats(3,j,k), stats(3,j,k)],
          "linestyle", ":")
    endfor
    set (hax, 'ylim' , xRange, 'ydir', 'reverse');
    title (labels(k));
    xlabel ("P(t)")
    xt = xticks ();
    xt = xt(2:end-1);
    xticks (xt);
    xticklabels (arrayfun (@(x) sprintf("%.g", x), xt, "UniformOutput", false));
    if (k == 1)
      yt = yticks ();
      yticklabels (arrayfun (@num2str, yt, "UniformOutput", false));
      ylabel ("t")
    else
      yticklabels ({});
    endif
  endfor
endfunction
