function plot_times (strBegin, vals1, strMid, vals2, strEnd, reverseDims = false, N = 20)
  ## -- plot_times (strStart, vals1, strEnd)
  ## -- plot_times (strStart, vals1, strEnd, N)
  ## -- plot_times (strStart, vals1, strMid, vals2, strEnd)
  ## -- load_times (strStart, vals1, strMid, vals2, strEnd, reverseDims)
  ## -- load_times (strStart, vals1, strMid, vals2, strEnd, reverseDims, N)
  ##      Plot pdf of times comming from different files with one column.
  ##
  ##      Further details can be found in the documentation of load_compacted.

  if (nargin < 3 || nargin > 7)
    error ("Incorrect number of input arguments.")
    return
  endif

  if (nargin == 3)
    [t, lgraph] = load_compacted (strBegin, vals1, strMid);
  elseif (nargin == 4)
    [t, lgraph] = load_compacted (strBegin, vals1, strMid);
    N = vals2;
  else
    [t, l1, lgraph] = load_compacted (strBegin, vals1, strMid, vals2, strEnd,
                        reverseDims);
  endif

  if (isempty (t))
    return
  endif
  [pdf, tmid, tstats] = get_pdf (t, 1, N);

  plot_pdfs (tmid, pdf, lgraph, tstats);
endfunction
