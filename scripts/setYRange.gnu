# setYRange.gnu
#
# This is free and unencumbered software released into the public domain.
# For more information, please refer to <https://unlicense.org>
#-------------------------------------------------------------------------------
# Usage: call "setYRange.gnu" [fileName directives [loopString [filterCondition]]]
#
# Motivation: 1. Having at least two labels on the axis tics is necessary to
#                understand which is the increment of each tic.
#                That is a common issue when using logscales in short ranges.
#             2. It is difficult to find useful a logscale for less than a
#                decade, commonly also for less than two decades.
#
# Solution provided: a. This script sets always a range with at least 2 labels
#                       (in mayor tics).
#                    b. If a yrange is not provided and data cover more than
#                       one decade, logscale is set.
#                    c. You can force not using logscale by specifying a yrange
#                       with less than two decades.
#                       (If yrange is provided and has two decades or more,
#                       logscale is set anyways)
#
# Description:
#
#   a. If yMin, yMax variables exist (read below about read_yMin_yMax for
#      full details):
#       1. set yr [yMin, yMax].
#       2. If yMin > 0 and [yMin, yMax] includes at least 2 decades:
#          "set log y" else "unset log y".
#
#   b. Else;
#       1. set yr [*:*].
#       2. Find yMin, yMax according to the arguments provided (see Arguments
#          below) and the current xrange. Then:
#          If yMin > 0 and [yMin, yMax] includes at least 1 decade:
#          "set log y" else "unset log y".
#
# Arguments: fileName: Filename of the file from which the data will be read.
#                      For example, "file1.sal".
#                      If they are several, use a string compatible with
#                      loopString such that when used as a macro it will
#                      be replaced by a valid filename (see below).
#
#            directives: Directives accepted by stats: `index`, `every`,
#                        and `using`. e.g.: "u 2" or "u ($1*$2)".
#
#            loopString: Use this if you want tho analyze several files.
#                        It will be used in the form "do @loopString {...}".
#                        fileName will be used as a macro if this parameter is
#                        passed. For example, you can pass fileName as
#                        "'fileBase'.i.'.txt'" and a loopString like
#                        "for [i in 'a b c d']" or "for [i=0:8]".
#
#            filterCondition: In the case you want to exclude some files from
#                             being considered, use this option. Setting it to
#                             "1" is equivalent to not specify it. Examples:
#                             "i < 5 && i > 7" or "f(i)", being f some defined
#                             function.
#
# Read variables:
#       * read_yMin_yMax: If it exists and it is equal to 0, the behavior is
#                         equivalent to yMin or yMax not being defined.
#                         Useful if calling setYRange successive times and you
#                         do not want to "undefine" variables before calls.
#
#       * yMin, yMax: Use these values to set the range instead of examining
#                     any data file, even if they are provided.
#                     If they do not exist, first and second argument are
#                     mandatory.
#
# Created/Modified variables:
#
#   In all cases:
#       * is_yMin_yMax_read: 1 or 0 whether values of previous values of yMin
#                            and yMax have been used.
#       * hasAnyFileBeenRead: 1 or 0 whether some file has been finally
#                             analysed.
#
#   In all cases, except if no file passed filterCondition (a warning is
#   printed in that case):
#       * isYLog: 1 or 0 whether "set logscale y" or "unset logscale y"
#                 was used.
#       * yMinReal: min(yMin, yMax). # Only interesting if yMin and yMax exist
#                                    # before setYRange is called but they both
#       * yMaxReal: max(yMin, yMax). # are always returned for consistency.
#
#   If yMin or yMax did not exist when setYRange was called or read_yMin_yMax
#   existed and was equal to 0:
#       * The variable indicated in loopString, if provided.
#       * STATS_*: Lot of variables returned by "stats". If loopString is
#                  provided, only the ones after processing the last file.
#       * yMin: minimum value of all the data as specified by directives.
#       * yMax: maximum value of all the data as specified by directives.
#
# Note: As a side-effect, if none of the read variables are defined and
#       setYRange is called, branch (a) of Description takes place.
#       If setYRange is called after that without changing any variable,
#       branch (b) applies. This second plot will have the Y range adjusted
#       to the minimal and maximum data values and a change from a possible
#       logscale to a non-logscale can happen.
#
#------------------------------------------------------------------------------
# Macro expansion does not see assignments of the variable inside a block.
# Setting a deffault here instead of inside the conditional.
if (ARGC == 3) {
    ARG4 = "1"
}

if (ARGC == 1 || ARGC >=5) {
    print "setYRange.gnu: Error: Wrong number of arguments.\n"
    print 'setYRange.gnu: Usage: call "setRanges.gnu" [fileName directives [loopString [filterCondition]]]'
    exit
}

# Find minimum and maximum value of the data
if (! exists("yMin") || ! exists("yMax") || (exists("read_yMin_yMax") && read_yMin_yMax == 0)) {
    if (ARGC == 0) {
        print "setYRange.gnu: Error: If yMin or yMax do not exist, fileName and directives are mandatory.\n"
        print 'setYRange.gnu: Usage: call "setRanges.gnu" [fileName directives [loopString [filterCondition]]]'
        exit
    }
    is_yMin_yMax_read = 0
    # "stats" filters data against both xrange and yrange.
    # set yrange free (xrange is under the responsability of the user).
    set yr [*:*]

    if (ARGC == 2) {
        hasAnyFileBeenRead = 1
        stats ARG1 @ARG2 nooutput
        yMin = STATS_min
        yMax = STATS_max
    } else {
        # Do not set/modify yMin, yMax if no file is going to pass the filter
        hasAnyFileBeenRead = 0
        do @ARG3 {
            if (@ARG4) {
                hasAnyFileBeenRead = 1
            }
        }
        if (hasAnyFileBeenRead) {
            # Set an invalid value for yMin, yMax.
            # (Note that they can exist before calling (possibly more than once)
            # this code)
            yMin = {0,1}
            yMax = {0,1}
            do @ARG3 {
                if (@ARG4) {
                    stats @ARG1 @ARG2 nooutput
                    if (yMin != {0,1} && yMax != {0,1}) {
                        yMin = (STATS_min < yMin) ? STATS_min : yMin
                        yMax = (STATS_max > yMax) ? STATS_max : yMax
                    } else {
                        yMin = STATS_min
                        yMax = STATS_max
                    }
                }
            }
        } else {
            print "setYRange.gnu: Warning: Y-range was not set. No file passed the filter."
        }
    }
} else {
    is_yMin_yMax_read = 1
    hasAnyFileBeenRead = 0
}

if (is_yMin_yMax_read || hasAnyFileBeenRead) {
    # At this point, yMin and yMax are always defined
    yMinReal = (yMin <= yMax) ? yMin : yMax
    yMaxReal = (yMin <= yMax) ? yMax : yMin

    if (is_yMin_yMax_read) {
        if (yMinReal > 0 && yMaxReal/yMinReal >= 100) {
            set log y
            isYLog = 1
        } else {
            unset log y
            isYLog = 0
        }
        set yr [yMin:yMax]
    } else {
        if (yMin > 0 && yMax/yMin >= 10) {
            set log y
            isYLog = 1
        } else {
            unset log y
            isYLog = 0
        }
        set yr [*:*]
    }
}
