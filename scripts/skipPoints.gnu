# skipInitialRows.gnu
#
# This is free and unencumbered software released into the public domain.
# For more information, please refer to <https://unlicense.org>
#------------------------------------------------------------------------------
#
# Note: For compatibility, this program uses stats' gnuplot command. If you
#       deal with huge files, efficiency really matters, etc., maybe you
#       prefer to use awk, sed or similar so you can return after the condition
#       is met and no unneeded statistics are calculated.
#
# Motivation:
#  * Some times it can be useful to skip the first rows of a file, until
#    certain condition is fulfilled. Even to not use a file at all if no row
#    meets it. That would avoid an error e.g. in "plot fileName u ($2>=100 ? $2
#    : 1/0)" if all values of second column are smaller than 100.
#  * The obvious case of use for this function is when it can be guaranteed
#    that if the condition is fulfilled for first time, it will be always met,
#    e.g., if condition is $2 > 100 and values of the second column are known
#    to be identical to a*$0, being a>0.
#  * Other times, it is possible that the condition is not met strictly for
#    every row after the first match, failing with bigger probability in rows
#    closer to the first match. For example, it is the case when there is a
#    tendency but with some noise. If you know in advance the typical magnitude
#    of the noise, you may specify a condition with a safety margin such that,
#    once the condition is satisfied, the rest of the rows are fine to be
#    included.
#  * If you need to choose interleaved rows, this is not your tool.
#
# Assumptions:
#
#  * It is expected that all read files have at least one record, but it
#    does not matter which records meet the condition.
#
# Usage: call "skipInitialRows.gnu" fileName condition LHS [loopString]
#
# Arguments: fileName: Filename in which condition will be checked.
#                      If they are several, use a string compatible with
#                      loopString (see below) such that when used as a macro
#                      it will be replaced by a valid filename.
#
#            condition: The condition to check. It is used in "using (...)", so
#                       anything compatible will work. For example, "$1+$2<9".
#
#            LHS: A string. Must be the name of a variable or a valid left hand
#                 side. If loopsSring is used, usually you will include the
#                 loop variables here to avoid overwritting, e.g., "B[i]"
#                 supposing that the loop variable is i.
#
#            loopString: Use this if you want tho analyze several files.
#                        It will be used in the form "do @loopString {...}".
#                        fileName will be used as a macro if this parameter is
#                        passed. For example, you can pass fileName as
#                        "'fileBase'.i.'.txt'" and a loopString like
#                        "for [i in 'a b c d']" or "for [i=0:8]".
#
# Description:
#   * If no loopString is provided:
#
#       If condition is satisfied at least by one row of provided file, the
#       index of first row which meets condition will be assigned to @LHS. Note
#       that provided index will have the same meaning than $0, so it does not
#       correspond with the line number of the file (lines with no records,
#       such as commentaries, are not counted).
#
#       If no row satisfies condition, -1*n is assigned to @LHS, where n is the
#       number of rows. This way, you can know the real number of skipped rows
#       (taking the absolute value) and whether all rows were skipped (smaller
#       than 0).
#
#   * If loopString is provided:
#
#       For every iteration indicated in loopString, @LHS will be assigned
#       by using the same rules than in the previous case. The intended use is
#       that both @LHS and @fileName will be different in each iteration.
#
# Read variables: None.
#
# Created/Modified variables:
#
#   * xrange and yrange are both set to [*:*] to avoid stats filtering any
#     record.
#
#   * someDataSkipped: 0 if all @LHS assignments are equal to 0. Else, 1.
#
#   * someDataConsidered: 0 if all @LHS assignments are negative. Else, 1.
#
# Implementation trick:
#  In my version of gnuplot, STATS_index_min is the first row with the minimum
#  value, and STATS_index_max the last row with the maximum value.
#  Then, for a non-marginal case, STATS_index_min would be the solution to the
#  problem if using "stats fileName u (condition ? 0 : 1)".
#  To not relay in that behavior, the following command is used instead:
#  "stats fileName (condition ? -$0 : $0)". Then, the solution is
#  STATS_index_max + 1, independently of stats implementation.
#
#  Note that if first row (or no row) satisties the condition, the mentioned
#  method is not valid. Code below will check that and will give a valid
#  result in any case.
#------------------------------------------------------------------------------

if (ARGC < 3 || ARGC > 4) {
    print "skipPoints.gnu: Error: Wrong number of arguments.\n"
    print 'skipPoints.gnu: Usage: call "skipPoints.gnu" fileName condition LHS [loopString]'
    exit
}

someDataSkipped = 0
someDataConsidered = 0
set xr [*:*]
set yr [*:*]

if (ARGC == 3) {
    undefine STATS_records
    stats ARG1 u (@ARG2 ? -$0 : $0) nooutput
    # Only first condition is needed in my gnuplot version, but the second does
    # not harm.
    if (! exists("STATS_records") || STATS_records == 0) {
        print 'skipPoints.gnu: Error: File '.ARG1.' has no data!'
        exit
    }
    if (STATS_index_max == 0) {
        someDataConsidered = 1
        @ARG3 = 0
    } else {
        someDataSkipped = 1
        if (STATS_index_min == 0) {
            @ARG3 = -STATS_records
        } else {
            someDataConsidered = 1
            @ARG3 = STATS_index_max + 1
        }
    }
} else {
    do @ARG4 {
        undefine STATS_records
        stats @ARG1 u (@ARG2 ? -$0 : $0) nooutput
        # Only first condition is needed in my gnuplot version, but the second
        # does not harm.
        if (! exists("STATS_records") || STATS_records == 0) {
            print 'skipPoints.gnu: Error: File '.@ARG1.' has no data!'
            exit
        }
        if (STATS_index_max == 0) {
            someDataConsidered = 1
            @ARG3 = 0
        } else {
            someDataSkipped = 1
            if (STATS_index_min == 0) {
                @ARG3 = -STATS_records
            } else {
                someDataConsidered = 1
                @ARG3 = STATS_index_max + 1
            }
        }
    }
}
