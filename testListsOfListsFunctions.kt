import kotlin.system.*

//---------------------
// Main classes
//---------------------

data class Shop(val name: String, val customers: List<Customer>)

data class Customer(val name: String, val city: String, val orders: List<Order>)

data class Order(val products: List<Product>, val isDelivered: Boolean)

data class Product(val name: String, val price: Double)

// --------------------------
// Testing functions
// --------------------------

fun usingFlatMap(customer: Customer): Product? =
        customer.orders.flatMap { it.products }
                .maxBy { it.price }

fun usingFlatMapWithSeq(customer: Customer): Product? =
        customer.orders.asSequence().flatMap { it.products.asSequence() }
                .maxBy { it.price }

fun Order.maxByPrice(): Product? = products.maxBy { it.price }

fun copyingMax2GrowingList(customer: Customer): Product? {
    val mostExpensiveProducts = mutableListOf<Product>()
    for (order in customer.orders) mostExpensiveProducts.add(order.maxByPrice()!!)
    return mostExpensiveProducts.maxBy { it.price }
}

fun copyingMax2InitializedList(customer: Customer): Product? {
    val mostExpensiveProducts = MutableList(customer.orders.size) {Product("Unvalid Product!", -1.0)}
    for (i in customer.orders.indices) mostExpensiveProducts[i] = customer.orders[i].maxByPrice()!!
    return mostExpensiveProducts.maxBy { it.price }
}

fun accumulator(customer: Customer): Product? {
    var product = Product("Unvalid Product!", -1.0)
    for (order in customer.orders) {
        val p = order.maxByPrice()!!
        if (p.price > product.price) product = p
    }
    return product
}

fun accumulatorFold(customer: Customer) =
        customer.orders.fold(Product("Unvalid Product!", -1.0)) {
            acc, order ->
            val p = order.maxByPrice()!!
            if (p.price > acc.price) p else acc
        }

data class Solution(val name: String, val f: (Customer) -> Product?)

val methods = listOf(
    Solution("usingFlatMap", ::usingFlatMap),
    Solution("copyingMax2GrowingList", ::copyingMax2GrowingList),
    Solution("copyingMax2InitializedList", ::copyingMax2InitializedList),
    Solution("accumulator", ::accumulator),
    Solution("accumulatorFold", ::accumulatorFold),
    Solution("usingFlatMapWithSeq", ::usingFlatMapWithSeq)
   )

// --------------------------
// Execution
// --------------------------

fun main(args: Array<String>) {
    // Parameters
    val nProducts = if (args.size >= 4) args[3].toInt() else 100
    val nOrders = if (args.size >= 3) args[2].toInt() else 100
    val nAverage = if (args.size >= 2) args[1].toInt() else 1
    val indexSolution = if (args.isNotEmpty()) args[0].toInt() else 0

    // Testing data
    val orders = List(nOrders) {
        Order(List(nProducts) {
            Product(kotlin.random.Random.nextDouble().toString(),
            kotlin.random.Random.nextDouble())
        }, true)
    }
    val customer1 = Customer("John", "Paris", orders)

    // Main part
    val nanosInOneMilli = 1E6
    var timeInMillis = -1.0
    for (iteration in 1..nAverage)
        timeInMillis = measureNanoTime { methods[indexSolution].f(customer1) }/nanosInOneMilli
    println(timeInMillis)
}
